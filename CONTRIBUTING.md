# Contributing to the Emilia Project - Emilia Garden

Please, see first the main version of the [Contributing file](https://gitlab.com/emilia-system/extra-files/blob/master/CONTRIBUTING.md).
 Here are things specific for this project if any is necessary.

## Top View

Here is how the program works.

### The Desktop Enviroments Classes

Each Desktop uses as a base a generic implementation, in with it changes 
configuration at the source, in the toolkit config file.

Every new Desktop that is added is a subclass of Desktop. In most cases it just
run the superclass original method and adds the desktop specifics on top.

``` mermaid
graph TB

  subgraph "Emilia Garden Library (lib/*.cpp except desktop.cpp)"
  Desktop/Generic --> Gnome 
  Desktop/Generic --> Kde/Plasma
  Desktop/Generic --> XFCE4
  Desktop/Generic --> Cinnamon
  Desktop/Generic --> Mate
  Desktop/Generic --> LXQT
  Desktop/Generic --> Budgie
  Desktop/Generic -- Root only --> Lightdm
  
end
```

The Desktops have methods to change:
- Gtk theme (2 and 3).
- Qt theme (via the Qt gadgets or the Kvantum theme if name coincides).
- Wallpaper image.
- Icon theme.
- Cursor theme.
- Font (In works right now. not even programed in).

### The command line program.

Emilia Garden has a command line interface, that connects with the library of desktops.
Note that the functions are mentioned after equals (ran out of ideas of symbols...).
``` mermaid
graph TB
  
  subgraph "Emilia Garden Command Line (command-line.cpp)"
  Input_From_Terminal -- Argument Processing = main part 1 to 3 --> Detect_Desktop
  Detect_Desktop -- Create Desktop Object = CreateInstance --> Check_Option
  Check_Option -- Execute Desktop Method = FunctionGet/Set --> Return

end
```
# TODO list for the project

Check the file progress.md for a status of the project now.

Also, completing this very file with details about DefaultFiles and common.cpp.
