# Emilia Garden
[![pipeline status](https://gitlab.com/emilia-system/emilia-garden/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-garden/commits/master)
[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)

Desktop customization is a common practice to many users. However, when multiples desktops are in use, sometimes nobody remembers how to change certain characteristics of a certain Desktop! With that in mind, this project was born to give control of all desktops on the hands of a tool with minimal dependencies and maximum usability.

# Installation

Go to the source folder (src) and execute make. If you wish to run in folder, update LD_LIBRARY_PATH to the current directory or install libemigarden.so on the folder /usr/lib/.

Warning - The garden.ini included is the Emilia System default. Change it to a empty or to something you want.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)

# Emilia Garden - pt_BR

Customizar um Desktop é uma prática comum a muitos usuários. Porém, quando se usam multiplos desktops, as vezes ninguém lembra como alterar certa característica de certo desktop! Com isso em mente, este projeto nasceu para dar o controle de todos os desktops na mão de uma ferramenta com o mínimo de dependências e máximo de usabilidade.

## Instalação

Vá na pasta fonte (src) e execute o make. Caso queira rodar ali mesmo, atualize o LD_LIBRARY_PATH para o diretório atual ou instale o libemigarden.so na pasta /usr/lib/.

Aviso - O garden.ini incluido é o padrão do Sistema Emilia. Mude para um vazio ou para algo que queira.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)
