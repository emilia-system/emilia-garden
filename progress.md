# Progress of the project

Possible values (Done parts):
 - set
 - get
 - all ready! (get and set)

The --- means that it uses the Generic solution. 
Nothing means that it's a TODO function.

| Functions         | Generic       | Gnome         | KDE/Plasma    | XFCE            | Cinnamon          | Mate             | LXQT               | Budgie        | Check (C++)    | Autocomplete   | Line Total |
| :-------------    | :------------ | :------------ | :------------ | :-------------- | :---------------- | :--------------- | :----------------- | :------------ | :------------- | :------------- | :--------- |
| GTK2 theme        | all ready!    | ---           | ---           | ---             | ---               | ---              | ---                | ---           | DONE!          | DONE!          | all 10     |
| GTK3 theme        | all ready!    | all ready!    | ---           | all ready!      | all ready!        | all ready!       | ---                | all ready!    | DONE!          | DONE!          | all 10     |
| GTK3 Dark theme   | all ready!    | ---           | ---           | ---             | ---               | ---              | ---                | --- or not?   | ---            | DONE!          | all 10     |
| QT5 Theme         | all ready!    | ---           | all ready!    | ---             | ---               | ---              | all ready!         | ---           | DONE!          | DONE!          | all 10     |
| Window M. theme   | Nothing to do |v uses DE.Theme|               |(xfwm4)          |(Muffin) all ready!|(Marco) all ready!|(openbox) all ready!| all ready!    | in progress... |                |      6     |
| Desktop E. theme  | Nothing to do | all ready!    |               |                 | all ready!        | uses gtk3  ^     |                    |               |                |                |      4     |
| Wallpaper         | all(feh)      | all ready!    |               |                 | all ready!        | all ready!       | all ready!         | Nothing...    |                |                |      5     |
| Icon theme        | all ready!    | all ready!    |               | get             | all ready!        | all ready!       | all ready!         | all ready!    | DONE!          | DONE!          |      8.5   |
| Cursor theme      | all ready!    | all ready!    | all ready!    | all ready!      | all ready!        | all ready!       | all ready!         | all ready!    | DONE!          | DONE!          | all 10     |
| Font/size         |               |               |               |                 |                   |                  |                    |               |                |                |     0/0    |

Numbers: 73.5/100

## Suggestions for the future:
 - Automatic reloading on changes. Not everything here changes on the instant.
 - Better feh usage. the generic wallpaper is a mess!
 - Wtf happened to Plasma icon themes!?
 - AND THE XFCE ICON THEMES!? XFCE is not bad...
