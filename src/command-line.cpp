#include <iostream>
#include <string>
#include <list>

//garden-libs ------------
#include <emilia-garden.h>

//language -------
#include <libintl.h>
#define _(STRING) gettext(STRING)
#define LOCALE "/usr/share/locale"

std::list<Glib::ustring> Valid_desktops   = {"generic","gnome","plasma","xfce","cinnamon","mate","lxqt","budgie"};
std::list<Glib::ustring> Valid_categories = {"gtk2-theme","gtk3-theme","gtk3-dark","qt5-theme","wm-theme","desktop-theme","wallpaper","icon-theme","cursor-theme","font"};


int usage(char* exit_message){
    std::cerr << exit_message << std::endl << std::endl;
    std::cout << _("Emilia-Garden usage:") << std::endl;
    std::cout <<   "    emilia-garden " << _("<operation> <desktop> <whatis> [value if operation is set]") << std::endl << std::endl;
    
    std::cout <<   "    " << _("Operations:") << std::endl;
    std::cout <<   "      get|set|list-all|reset|help - " << _("list-all and help don't use any more arguments") << ". " << _("But reset accepts a Desktop.") <<std::endl;
    std::cout <<   "    " << _("Desktop:") << std::endl;
    std::cout <<   "      current|all|generic|gnome|plasma|xfce|cinnamon|mate|lxqt|budgie" << std::endl;
    std::cout <<   "      current - " << _("Tries to detect what desktop are you running") << std::endl; 
    std::cout <<   "      generic - " << _("Tries to apply a generic solution, with may or may not work.") << std::endl;
    
    std::cout <<   "    " << _("Whatis:") << std::endl;
    std::cout <<   "      gtk2-theme|gtk3-theme|gtk3-dark|qt5-theme|wm-theme|desktop-theme|wallpaper|icon-theme|cursor-theme|font." << std::endl;
    std::cout <<   "      " << _("Choose what you want to alter.") << std::endl << std::endl;

    exit(1);
    return 1;
}

Glib::ustring FunctionGet(const char* whatIs, EmiliaGarden::Desktop* desktop);
void FunctionSet(const char* whatIs, EmiliaGarden::Desktop* desktop, Glib::ustring set_argument);
void PrintAll(Glib::ustring exit);
std::list<Glib::ustring> split_string(Glib::ustring input,Glib::ustring token);

EmiliaGarden::Desktop* CreateInstance(const char* desktop){
    if     (strcmp(desktop,"current")  == 0 ) {
        return CreateInstance(EmiliaGarden::DetectDesktop().c_str());
    }
    else if(strcmp(desktop,"generic")  == 0 ) return new EmiliaGarden::Desktop();
    else if(strcmp(desktop,"gnome")    == 0 ) return new EmiliaGarden::Gnome();
    else if(strcmp(desktop,"plasma")   == 0 ) return new EmiliaGarden::Plasma();
    else if(strcmp(desktop,"xfce")     == 0 ) return new EmiliaGarden::Xfce();
    else if(strcmp(desktop,"cinnamon") == 0 ) return new EmiliaGarden::Cinnamon();
    else if(strcmp(desktop,"mate")     == 0 ) return new EmiliaGarden::Mate();
    else if(strcmp(desktop,"lxqt")     == 0 ) return new EmiliaGarden::Lxqt();
    else if(strcmp(desktop,"budgie")   == 0 ) return new EmiliaGarden::Budgie();
    else if(strcmp(desktop,"lightdm")  == 0 ) return new EmiliaGarden::Lightdm(); // secret "desktop".
    else usage(_("Invalid desktop"));
    return new EmiliaGarden::Desktop();
}

void list_all(){
    Glib::ustring exit = "";
    for (std::string category : Valid_categories){
            std::cout << category << ":" << std::endl; 
            for (Glib::ustring desktop : Valid_desktops){
                exit += desktop + ";" + FunctionGet(category.c_str(),CreateInstance(desktop.c_str()))  + ";\n";
            }
            PrintAll(exit);
            std::cout << std::endl;
        }
}
int reset(Glib::ustring desktop){
    if(strcmp(desktop.c_str(),"all") == 0) {
        return usage(_("All is not supported with Reset, select a desktop"));
    }
    Glib::ustring config = EmiliaGarden::Create::Garden();
    Glib::KeyFile initFile = Glib::KeyFile();
    try{
        initFile.load_from_file(config,Glib::KEY_FILE_KEEP_TRANSLATIONS);
    }
    catch(Glib::KeyFileError& e){
        return usage(_("Invalid or Problematic Garden config file"));
    }
    
    for(std::string category : Valid_categories){
        try{
            //specific to desktop
            FunctionSet(category.c_str(), CreateInstance(desktop.c_str()), initFile.get_string(desktop,category));
        }
        catch(Glib::KeyFileError& e)
        {
            if (e.code() != Glib::KeyFileError::Code::KEY_NOT_FOUND && e.code() != Glib::KeyFileError::Code::GROUP_NOT_FOUND)
            {
                std::cerr << e.what() << std::endl;
                return usage(_(" - Unknown Error in reset config file"));
            }
            else{
                //no specific, try the default
                try{
                    FunctionSet(category.c_str(), CreateInstance(desktop.c_str()), initFile.get_string("generic",category)) ;
                }
                catch(Glib::KeyFileError& e){
                    //can only be the key or group not found. so... ignore-it.
                    std::cerr << category << _(" - There is no default in Reset. Ignoring...")<< std::endl;
                }
            }
        }
    }
    
    return 0;
}

int main(int argc, char *argv[]) {
    // i18n setup
    std::locale::global(std::locale(""));
    bindtextdomain("emilia-garden", LOCALE);
    textdomain("emilia-garden");
    
    Glib::init(); //Needed for dBus functions.
    Gio::init();  //Needed
    
    bool isAll   = false;
    bool isSet   = false;
    bool isReset = false;
    
    Glib::ustring set_argument;
    
//1 - help/get/set/list-all/reset
//Process first argument
    if (argc < 2){
        return usage(_("Please specify arguments"));
    }
    
    if(strcmp(argv[1],"help") == 0) { 
        usage(_("Emilia-Garden: Customize your desktop!")); 
        return 0;
    }
    if(strcmp(argv[1],"list-all") == 0) {
        list_all();
        return 0;
    }
    else if (strcmp(argv[1],"get") == 0)   { isSet   = false; }
    else if (strcmp(argv[1],"set") == 0)   { isSet   = true;  }
    else if (strcmp(argv[1],"reset") == 0) { isReset = true;  }
    else return usage(_("Operation Not Found"));
    
//2 - Desktop enviroment - Can be any of valid desktops
//Process second argument
    if (argc < 3){ return usage(_("Please specify what desktop to manipulate")); }
    if (isReset ) {
        return reset(argv[2]);
    }
    
    // The validation of the desktops happens in the CreateInstance function.
    
//Process third argument
//3 - gtk2-theme gtk3-theme gtk3-dark qt5-theme wm-theme desktop-theme wallpaper icon-theme cursor-theme font
    if(argc < 4){ return usage(_("Please specify what thing to set/get")); }
    //first, see if there is a argument
    if(isSet){
        if(argc < 5){ return usage(_("Missing set argument")); }
        set_argument = argv[4];
    }
    
    Glib::ustring exit;
    if(strcmp(argv[2],"all") == 0){
        if(isSet)
        {
            for (std::string desktop : Valid_desktops)
                FunctionSet(argv[3], CreateInstance(desktop.c_str()), set_argument);
        }
        else
        {
            exit = "";
            for (std::string desktop : Valid_desktops)
                exit += desktop + ";" + FunctionGet(argv[3],CreateInstance(desktop.c_str()))  + ";\n";
            PrintAll(exit);
        }
    }
    else {
        if (isSet) FunctionSet(argv[3],CreateInstance(argv[2]),set_argument);
        else {
            exit = FunctionGet(argv[3],CreateInstance(argv[2]));
            std::cout << exit << std::endl;
        }
    }
    return 0;
}

Glib::ustring FunctionGet(const char* whatIs, EmiliaGarden::Desktop* desktop){
    try{
        if     (strcmp(whatIs,"gtk2-theme") == 0){
            return desktop->GetGtk2Theme();
        }
        else if(strcmp(whatIs,"gtk3-theme") == 0){
            return desktop->GetGtk3Theme();
        }
        else if(strcmp(whatIs,"gtk3-dark") == 0){
            return (desktop->GetGtk3Dark()? "True":"False");
        }
        else if(strcmp(whatIs,"qt5-theme") == 0){
            return desktop->GetQt5Theme();
        }
        else if(strcmp(whatIs,"wm-theme") == 0){
            return desktop->GetWMTheme();
        }
        else if(strcmp(whatIs,"desktop-theme") == 0){
            return desktop->GetDesktopTheme();
        }
        else if(strcmp(whatIs,"wallpaper") == 0){
            return desktop->GetWallpaper();
        }
        else if(strcmp(whatIs,"icon-theme") == 0){
            return desktop->GetIconTheme();
        }
        else if(strcmp(whatIs,"cursor-theme") == 0){
            return desktop->GetCursorTheme();
        }
        else if(strcmp(whatIs,"font") == 0){
            return desktop->GetFont();
        }
        else usage(_("Invalid category"));
    }
    catch (EmiliaGarden::InvalidDBusCode& e){
        std::cerr << desktop->GetName() << ": " << _("Missing Dbus Dependency for what provides ") << e.what() << ". " << _("Check optional dependencies for help") << std::endl;
    }
    catch (EmiliaGarden::KeyGroupNotFound& e){
        std::cerr << desktop->GetName() << ": Group-Key " << e.what() << " " << _("not found.") << std::endl;
    }
    catch (EmiliaGarden::NotInstalledProgram& e){
        std::cerr << desktop->GetName() << ": " << _("Program missing from the dependencies: ")<< e.what() << ". " << _("Check optional dependencies for help") << std::endl;
    }
    catch (EmiliaGarden::InvalidXml& e){
        std::cerr << desktop->GetName() << ": " << _("There is an error with the Xml ")<< e.what() << ". " << _("Try deleting it and try again.") << std::endl;
    }
    catch (Glib::KeyFileError& e){
        std::cerr << desktop->GetName() << ": Glib " << e.what() << std::endl;
    }
    catch (Gio::Error& e){
        std::cerr << desktop->GetName() << ": Gio error " << e.what() << std::endl;
    }
    return "";
}

void FunctionSet(const char* whatIs, EmiliaGarden::Desktop* desktop, Glib::ustring set_argument){
    try{
        if     (strcmp(whatIs,"gtk2-theme") == 0){
            desktop->SetGtk2Theme(set_argument);
        }
        else if(strcmp(whatIs,"gtk3-theme") == 0){
            desktop->SetGtk3Theme(set_argument);
        }
        else if(strcmp(whatIs,"gtk3-dark") == 0){
            desktop->SetGtk3Dark((set_argument.lowercase().compare("true") == 0) ? true : false);
        }
        else if(strcmp(whatIs,"qt5-theme") == 0){
            desktop->SetQt5Theme(set_argument);
        }
        else if(strcmp(whatIs,"wm-theme") == 0){
            desktop->SetWMTheme(set_argument);
        }
        else if(strcmp(whatIs,"desktop-theme") == 0){
            desktop->SetDesktopTheme(set_argument);
        }
        else if(strcmp(whatIs,"wallpaper") == 0){
            desktop->SetWallpaper(set_argument);
        }
        else if(strcmp(whatIs,"icon-theme") == 0){
            desktop->SetIconTheme(set_argument);
        }
        else if(strcmp(whatIs,"cursor-theme") == 0){
            desktop->SetCursorTheme(set_argument);
        }
        else if(strcmp(whatIs,"font") == 0){
            desktop->SetFont(set_argument);
        }
        else usage(_("Invalid category"));
        
        return;
    }
    catch (EmiliaGarden::InvalidTheme& e){
        std::cerr << desktop->GetName() << ": " << _("Invalid theme for ") <<  whatIs << "." << std::endl;
    }
    catch (EmiliaGarden::NotRootException& e){
        std::cerr << desktop->GetName() << ": " << _("Root is needed for") << " set " << whatIs << " " << _("Try run it with sudo or as root.") << std::endl;
    }
    catch (EmiliaGarden::InvalidDBusCode& e){
        std::cerr << desktop->GetName() << ": " << _("Missing Dbus Dependency for what provides ")<< e.what() << ". " << _("Check optional dependencies for help") << std::endl;
    }
    catch (EmiliaGarden::KeyGroupNotFound& e){
        std::cerr << desktop->GetName() << ": Group-Key " << e.what() << " " << _("not found.") << std::endl;
    }
    catch (EmiliaGarden::NotInstalledProgram& e){
        std::cerr << desktop->GetName() << ": " << _("Program missing from the dependencies: ")<< e.what() << ". " << _("Check optional dependencies for help") << std::endl;
    }
    catch (EmiliaGarden::InvalidXml& e){
        std::cerr << desktop->GetName() << ": " << _("There is an error with the Xml ")<< e.what() << ". " << _("Try deleting it and try again.") << std::endl;
    }
}

void PrintAll(Glib::ustring exit){
    bool weirdBug=true;
    bool isDesktopName=true;
    for(Glib::ustring part : split_string(exit,";")){
        if(isDesktopName) {
            std::cout << part;
            for(int i = 0; i < 14-part.length(); i++) std::cout << " ";
            isDesktopName=false;
            if(!weirdBug) {
                std::cout << " ";
            }
            else weirdBug = false;
        }
        else {
            if (part.empty()) std::cout << _("No Results");
            else std::cout << part;
            isDesktopName=true;
        }
    }
    std::cout << std::endl;
}

std::list<Glib::ustring> split_string(Glib::ustring input,Glib::ustring token){
    Glib::ustring rest = input;
    
    std::list<Glib::ustring> ret;
    std::list<Glib::ustring>::iterator it = ret.begin();
    
    int pos;
    while(true){
        pos = rest.find(token);
        if(pos == -1) {
            //last one. 
            ret.insert(it,rest.substr(0,pos));
            break;
        }
        ret.insert(it,rest.substr(0,pos));
        rest = rest.substr(pos+token.length());
    }
    return ret;
}
