#ifndef EMILIA_GARDEN_H
#define EMILIA_GARDEN_H

/*Utils*/
#include <unistd.h>           //geteuid
#include <glibmm/fileutils.h> //File operations
#include <glibmm/ustring.h>   //Better std::string
#include <glibmm/miscutils.h> //Common operations, like get enviroment or user home dir/config dir
#include <glibmm/init.h>      //Needed to make Dbus work without errors
#include <giomm/init.h>       //Needed to make some things work without errors
#include <glibmm/regex.h>     //Regular expressions

/*True Backend*/
#include <glibmm/keyfile.h> //INI files
#include <giomm/settings.h> //DBUS
#include <giomm/file.h>     //FileSystem
  /*XML by libxml++*/
#include <libxml++/parsers/domparser.h> //DomParser class
#include <libxml++/nodes/contentnode.h> //ContentNode. pulls Normal Node.

namespace EmiliaGarden 
{
    /// @brief The Desktop Class exists as a base and also as a Generic implementation of everyone.
    /// 
    /// If a desktop don't change the way it deals with the parts like gtk3 or qt, it uses
    /// the original implementation defined here.
    ///
    /// The configuration altered differ if the program is being run as root (or sudo).
    /// In root/sudo, the change is made for new users, so new users have all the configurations ready for them.
    /// In normal usage, the user that runs it is the one that will have new configurations.
    /// 
    /// There is no way to alter the root's configurations. You shouldn't use root for graphical anyway.
    ///
    class Desktop{
    public:
        virtual Glib::ustring GetName               ();
        
        virtual void          SetGtk2Theme          (Glib::ustring theme_name);
        virtual Glib::ustring GetGtk2Theme          ();        
        virtual bool          isValidGtk2Theme      (Glib::ustring theme_name, Glib::ustring* location);

        virtual void          SetGtk3Theme          (Glib::ustring theme_name);
        virtual Glib::ustring GetGtk3Theme          ();
        virtual bool          isValidGtk3Theme      (Glib::ustring theme_name);
        
        virtual void          SetGtk3Dark           (bool isDark);
        virtual bool          GetGtk3Dark           ();
        
        virtual void          SetQt5Theme           (Glib::ustring theme_name);
        virtual Glib::ustring GetQt5Theme           ();
        virtual bool          isValidQt5Widget      (Glib::ustring theme, bool isLxqt);
        virtual bool          isValidKvantumTheme   (Glib::ustring theme);
        
        virtual void          SetWMTheme            (Glib::ustring theme_name);
        virtual Glib::ustring GetWMTheme            ();
        virtual bool          isValidWMTheme        (Glib::ustring theme_name);
        
        virtual void          SetDesktopTheme       (Glib::ustring theme_name);
        virtual Glib::ustring GetDesktopTheme       ();
        virtual bool          isValidDesktopTheme   (Glib::ustring theme_name);

        virtual void          SetWallpaper          (Glib::ustring wallpaper_path);
        virtual Glib::ustring GetWallpaper          ();
        virtual bool          isValidWallpaper      (Glib::ustring wallpaper_path, bool haveXml);

        virtual void          SetIconTheme          (Glib::ustring theme_name);
        virtual Glib::ustring GetIconTheme          ();
        virtual bool          isValidIconTheme      (Glib::ustring theme_name);
        
        virtual void          SetCursorTheme        (Glib::ustring theme_name);
        virtual Glib::ustring GetCursorTheme        ();
        virtual bool          isValidCursorTheme    (Glib::ustring theme_name);
        
        virtual void          SetFont               (Glib::ustring font_name, int size = 10, Glib::ustring style = "Regular");
        virtual Glib::ustring GetFont               ();
        virtual bool          isValidFont           (Glib::ustring font_name);
    };
    
    Glib::ustring  DetectDesktop            ();
    bool           isRoot                   ();
    
    bool           isQt5ctInstalled         ();
    bool           isKvantumInstalled       ();
    
    Glib::ustring  checkPathProtocol        (Glib::ustring path);
    
    namespace Init {
        Glib::ustring GetString             (Glib::ustring filename, Glib::ustring group, Glib::ustring key);
        void          SetString             (Glib::ustring filename, Glib::ustring group, Glib::ustring key, Glib::ustring value);
        int           GetInt                (Glib::ustring filename, Glib::ustring group, Glib::ustring key);
        void          SetInt                (Glib::ustring filename, Glib::ustring group, Glib::ustring key, int value);
        bool          GetBool               (Glib::ustring filename, Glib::ustring group, Glib::ustring key);
        void          SetBool               (Glib::ustring filename, Glib::ustring group, Glib::ustring key, bool value);
    }
    namespace Dbus {
        bool          isValidScheme         (Glib::ustring scheme);
        Glib::ustring CreateDbusFile        ();
        Glib::ustring GetString             (Glib::ustring scheme, Glib::ustring key);
        int           GetInt                (Glib::ustring scheme, Glib::ustring key);
        bool          GetBool               (Glib::ustring scheme, Glib::ustring key);
        void          SetString             (Glib::ustring scheme, Glib::ustring key, Glib::ustring value);
        void          SetInt                (Glib::ustring scheme, Glib::ustring key, int value);
        void          SetBool               (Glib::ustring scheme, Glib::ustring key, bool value);
    }
    namespace Xml {
        void          SetXml                (Glib::ustring file_path, Glib::ustring xPath, Glib::ustring value);
        Glib::ustring GetXml                (Glib::ustring file_path, Glib::ustring xPath);
    }
    namespace Create {
        Glib::ustring Garden                ();
        Glib::ustring Gtk2                  ();
        Glib::ustring Gtk3                  ();
        Glib::ustring Qtct                  ();
        Glib::ustring Kvantum               ();
        Glib::ustring KdeGlobals            ();
        Glib::ustring KdePlasmarc           ();
        Glib::ustring KdeWinrc              ();
        Glib::ustring Kcminputrc            ();
        Glib::ustring XfceXsettings         ();
        Glib::ustring XfceXfwm              ();
        Glib::ustring LxqtConfig            ();
        Glib::ustring LxqtSession           ();
        Glib::ustring LxqtSettings          ();
        Glib::ustring Openbox               (bool isLxqt);
        Glib::ustring Feh                   ();
        Glib::ustring DefaultCursor         ();
        Glib::ustring LightdmGtkGreeter     ();
    }
    
    namespace Gtk2 {
        void          SetValue              (Glib::ustring key, Glib::ustring value);
        Glib::ustring GetValue              (Glib::ustring key);
    }
    Glib::ustring     GetGtkFont            (bool isGtk2);
    
//-------------------------------------------------------------------------------------

    class Gnome : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetGtk3Theme          (Glib::ustring theme_name);
        Glib::ustring GetGtk3Theme          ();
    
        void          SetWMTheme            (Glib::ustring theme_name);
        Glib::ustring GetWMTheme            ();
        bool          isValidWMTheme        (Glib::ustring theme_name);
    
        void          SetDesktopTheme       (Glib::ustring theme_name);
        Glib::ustring GetDesktopTheme       ();
        bool          isValidDesktopTheme   (Glib::ustring theme_name);
        
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
    };
    class Plasma : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetQt5Theme           (Glib::ustring theme_name);
        Glib::ustring GetQt5Theme           ();
    
        void          SetWMTheme            (Glib::ustring theme_name);
        Glib::ustring GetWMTheme            ();
        bool          isValidWMTheme        (Glib::ustring theme_name);
    
        void          SetDesktopTheme       (Glib::ustring theme_name);
        Glib::ustring GetDesktopTheme       ();
        bool          isValidDesktopTheme   (Glib::ustring theme_name);
        
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
    };
    class Xfce : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetGtk3Theme          (Glib::ustring theme_name);
        Glib::ustring GetGtk3Theme          ();
    
        void          SetWMTheme            (Glib::ustring theme_name);
        Glib::ustring GetWMTheme            ();
        bool          isValidWMTheme        (Glib::ustring theme_name);
    
        void          SetDesktopTheme       (Glib::ustring theme_name);
        Glib::ustring GetDesktopTheme       ();
        bool          isValidDesktopTheme   (Glib::ustring theme_name);
        
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
    };
    class Cinnamon : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetGtk3Theme          (Glib::ustring theme_name);
        Glib::ustring GetGtk3Theme          ();
    
        void          SetWMTheme            (Glib::ustring theme_name);
        Glib::ustring GetWMTheme            ();
        bool          isValidWMTheme        (Glib::ustring theme_name);
    
        void          SetDesktopTheme       (Glib::ustring theme_name);
        Glib::ustring GetDesktopTheme       ();
        bool          isValidDesktopTheme   (Glib::ustring theme_name);
        
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
    };
    class Mate : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetGtk3Theme          (Glib::ustring theme_name);
        Glib::ustring GetGtk3Theme          ();
        
        void          SetWMTheme            (Glib::ustring theme_name);
        Glib::ustring GetWMTheme            ();
        bool          isValidWMTheme        (Glib::ustring theme_name);
    
        void          SetDesktopTheme       (Glib::ustring theme_name);
        Glib::ustring GetDesktopTheme       ();
        bool          isValidDesktopTheme   (Glib::ustring theme_name);
        
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
    };
    class Lxqt : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetQt5Theme           (Glib::ustring theme_name);
        Glib::ustring GetQt5Theme           ();
    
        void          SetWMTheme            (Glib::ustring theme_name);
        Glib::ustring GetWMTheme            ();
        bool          isValidWMTheme        (Glib::ustring theme_name);
    
        void          SetDesktopTheme       (Glib::ustring theme_name);
        Glib::ustring GetDesktopTheme       ();
        bool          isValidDesktopTheme   (Glib::ustring theme_name);
        
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
    };
    class Budgie : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetGtk3Theme          (Glib::ustring theme_name);
        Glib::ustring GetGtk3Theme          ();
        
        void          SetWMTheme            (Glib::ustring theme_name);
        Glib::ustring GetWMTheme            ();
        bool          isValidWMTheme        (Glib::ustring theme_name);
    
        void          SetDesktopTheme       (Glib::ustring theme_name);
        Glib::ustring GetDesktopTheme       ();
        bool          isValidDesktopTheme   (Glib::ustring theme_name);
        
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
    };

    //NOT REALLY DESKTOPS BUT CAN USE THE CODE
    //set is root only!!
    class Lightdm : public Desktop{
        Glib::ustring GetName               ();
        
        void          SetGtk3Theme          (Glib::ustring theme_name);
        Glib::ustring GetGtk3Theme          ();
    
        void          SetWallpaper          (Glib::ustring wallpaper_path);
        Glib::ustring GetWallpaper          ();
        
        void          SetIconTheme          (Glib::ustring theme_name);
        Glib::ustring GetIconTheme          ();
          
        void          SetCursorTheme        (Glib::ustring theme_name);
        Glib::ustring GetCursorTheme        ();
        
        void          SetFont               (Glib::ustring font_name, int size = 10, Glib::ustring style = "Regular");
        Glib::ustring GetFont               ();
        
        //Overriten functions. do NOT use them. 
        void          SetGtk2Theme          (Glib::ustring theme_name);
        Glib::ustring GetGtk2Theme          ();        
        void          SetGtk3Dark           (bool isDark);
        bool          GetGtk3Dark           ();
        void          SetQt5Theme           (Glib::ustring theme_name);
        Glib::ustring GetQt5Theme           ();
    };
    
    //EXCEPTIONS
    class InvalidDBusCode : public std::runtime_error {
    public:
        InvalidDBusCode(std::string& DbusPath) : std::runtime_error(DbusPath){};
        InvalidDBusCode(const char*  DbusPath) : std::runtime_error(DbusPath){};
    };

    class NotInstalledProgram : public std::runtime_error {
    public:
        NotInstalledProgram(std::string& ProgramPath) : std::runtime_error(ProgramPath){};
        NotInstalledProgram(const char*  ProgramPath) : std::runtime_error(ProgramPath){};
    };
    
    class KeyGroupNotFound : public std::runtime_error {
    public:
        KeyGroupNotFound(std::string& Group_key) : std::runtime_error(Group_key){};
        KeyGroupNotFound(const char*  Group_key) : std::runtime_error(Group_key){};
    };
    class InvalidTheme : public std::exception {
    public:
        InvalidTheme() : std::exception(){};
    };
    class NotRootException : public std::exception {
    public:
        NotRootException() : std::exception(){};
    };
    class InvalidXml : public std::runtime_error {
    public:
        InvalidXml(std::string& xml_path) : std::runtime_error(xml_path){};
        InvalidXml(const char*  xml_path) : std::runtime_error(xml_path){};
    };
    class NoDefaultFileException : public std::runtime_error {
    public:
        NoDefaultFileException(std::string& missing_file) : std::runtime_error(missing_file){};
        NoDefaultFileException(const char*  missing_file) : std::runtime_error(missing_file){};
    };
}
#endif
