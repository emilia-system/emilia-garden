#!/bin/bash

if ! [[ ${DESKTOP_SESSION} == "plasma" || ${DESKTOP_SESSION} == "lxqt" ]]; then 
    export QT_QPA_PLATFORMTHEME="qt5ct"; 
else 
    unset QT_QPA_PLATFORMTHEME; 
fi 
#Needed for Emilia-Garden package Qt configuration
