#include <emilia-garden.h>

namespace EmiliaGarden{
    
    //FUNCTIONS
    inline bool isRoot(){
        return (geteuid() == 0);
    }
    
    Glib::ustring DetectDesktop (){return Glib::getenv("DESKTOP_SESSION");}
    
    bool isQt5ctInstalled(){
        return Glib::file_test("/usr/bin/qt5ct", Glib::FILE_TEST_EXISTS);
    }
    bool isKvantumInstalled(){
        return Glib::file_test("/usr/lib/qt/plugins/styles/libkvantum.so", Glib::FILE_TEST_EXISTS);
    }
    
    Glib::ustring checkPathProtocol(Glib::ustring path){
        if( path.substr(0,7).compare("file://") == 0) {
            return path; // It has the file:// prefix. return itself
        }
        else return "file://" + path;
    }
    
    //FILESYSTEM AUX.
    namespace {
        void FileCreate(Glib::ustring origin_path, Glib::ustring destiny_path, Glib::ustring file){
            if (!Glib::file_test(destiny_path,Glib::FILE_TEST_IS_DIR)){
                if (Glib::file_test(destiny_path,Glib::FILE_TEST_EXISTS)){
                    //error. is a file and not a dir. should be eliminated.
                    Gio::File::create_for_path(destiny_path)->remove();
                }
                //dir doesn't exist. create
                Glib::RefPtr<Gio::File> destiny_dir = Gio::File::create_for_path(destiny_path);
                destiny_dir->make_directory_with_parents(); // nothing for no cancellable 
            }
            
            Glib::RefPtr<Gio::File> SourcePointer = Gio::File::create_for_path(origin_path + file);
            Glib::RefPtr<Gio::File> DestPointer = Gio::File::create_for_path(destiny_path + file);
            
            SourcePointer->copy(DestPointer,Gio::FILE_COPY_OVERWRITE);
        }

        Glib::ustring FileGetContents(Glib::RefPtr<Gio::File> file){
            auto stream = file->read();

            /*Count file char size
                WARNING -> in windows, a newline counts as 2, in linux count as 1, so more testing is needed.
            */
            stream->seek(0,Glib::SEEK_TYPE_END);
            const auto file_size = stream->tell();
            stream->seek(0,Glib::SEEK_TYPE_SET);
            
            /*alloc the char necessary and read it.*/
            gchar buffer[file_size+2];
            memset(buffer, 0, file_size+2);
            /*const gsize bytes_read = */ stream->read(buffer, file_size+2);
            
            /*put in contents*/
            Glib::ustring contents;
            contents.assign(buffer);
            return contents;
        }

        void FileReplace (Glib::ustring file_path, Glib::ustring search, Glib::ustring replace){
            auto file = Gio::File::create_for_path(file_path);
            
            Glib::ustring contents = FileGetContents(file);
            
            //-----REPLACE IN REGEX. CONTENTS IN 'contents'
            Glib::RefPtr<Glib::Regex> reg = Glib::Regex::create(search);
            contents = reg->replace(contents,0,replace,Glib::REGEX_MATCH_NOTEOL);
            
            //-----SAVE BACK TO THE FILE
            std::string newFlag; //Yeah, useless for now.
            file->replace_contents(contents,std::string(""),newFlag);
        }

        void FileAdd(Glib::ustring file_path, Glib::ustring string_to_add, bool isEnd){
            //-----OPENFILE
            auto file = Gio::File::create_for_path(file_path);
            
            Glib::ustring contents = FileGetContents(file);
            
            //-----SAVE BACK TO THE FILE
            std::string newFlag; //Yeah, useless for now.
            if(isEnd) file->replace_contents(string_to_add + contents,std::string(""),newFlag);
            else      file->replace_contents(contents + string_to_add,std::string(""),newFlag);
        }

        Glib::ustring FileMatch(Glib::ustring file_path, Glib::ustring search){
            auto file = Gio::File::create_for_path(file_path);
            Glib::ustring contents = FileGetContents(file);
            
            Glib::MatchInfo info;
            auto reg = Glib::Regex::create(search);
            
            if (!reg->match(contents,info)) return "";
            
            return info.fetch(0);
        }
    }

    //INIT Code
    namespace Init {
        Glib::ustring GetString (Glib::ustring filename, Glib::ustring group, Glib::ustring key){
            Glib::KeyFile initFile = Glib::KeyFile();
            initFile.load_from_file(filename,Glib::KEY_FILE_KEEP_TRANSLATIONS);
            try{
                return initFile.get_string(group,key);
            }
            catch(Glib::KeyFileError& e) 
            {
                if (e.code() == Glib::KeyFileError::Code::KEY_NOT_FOUND || e.code() == Glib::KeyFileError::Code::GROUP_NOT_FOUND)
                {   
                    std::string error (group + " - " + key);
                    throw KeyGroupNotFound(error);
                }
            }
            return "";
        }
        void SetString(Glib::ustring filename, Glib::ustring group, Glib::ustring key, Glib::ustring value){
            Glib::KeyFile initFile = Glib::KeyFile();
            
            try{
                initFile.load_from_file(filename,Glib::KEY_FILE_KEEP_TRANSLATIONS);
                initFile.set_string(group,key,value);
                initFile.save_to_file(filename);
            }
            catch(Glib::KeyFileError& e) 
            {
                //fix for KDE's key[$e] and similar.
                if(e.code() == Glib::KeyFileError::Code::INVALID_VALUE){
                    FileReplace(filename,"$","___");
                    SetString(filename, group, key, value);
                    FileReplace(filename,"___","$");
                }
                else if (e.code() == Glib::KeyFileError::Code::KEY_NOT_FOUND || e.code() == Glib::KeyFileError::Code::GROUP_NOT_FOUND)
                {   
                    std::string error (group + " - " + key);
                    throw KeyGroupNotFound(error);
                }
            }
        }
        int GetInt  (Glib::ustring filename, Glib::ustring group, Glib::ustring key){
            Glib::KeyFile initFile = Glib::KeyFile();
            initFile.load_from_file(filename,Glib::KEY_FILE_KEEP_TRANSLATIONS);
            try{
                return initFile.get_integer(group,key);
            }
            catch(Glib::KeyFileError& e) 
            {
                if (e.code() == Glib::KeyFileError::Code::KEY_NOT_FOUND || e.code() == Glib::KeyFileError::Code::GROUP_NOT_FOUND)
                {   
                    std::string error (group + " - " + key);
                    throw KeyGroupNotFound(error);
                }
            }
            return -1;
        }
        void SetInt  (Glib::ustring filename, Glib::ustring group, Glib::ustring key, int value){
            Glib::KeyFile initFile = Glib::KeyFile();
            initFile.load_from_file(filename,Glib::KEY_FILE_KEEP_TRANSLATIONS);
            initFile.set_integer(group,key,value);
            initFile.save_to_file(filename);
        }
        bool GetBool (Glib::ustring filename, Glib::ustring group, Glib::ustring key){
            Glib::KeyFile initFile = Glib::KeyFile();
            initFile.load_from_file(filename,Glib::KEY_FILE_KEEP_TRANSLATIONS);
            try{
                return initFile.get_boolean(group,key);
            }
            catch(Glib::KeyFileError& e) 
            {
                if (e.code() == Glib::KeyFileError::Code::KEY_NOT_FOUND || e.code() == Glib::KeyFileError::Code::GROUP_NOT_FOUND)
                {   
                    std::string error (group + " - " + key);
                    throw KeyGroupNotFound(error);
                }
            }
            return false;
        }
        void SetBool (Glib::ustring filename, Glib::ustring group, Glib::ustring key, bool value){
            Glib::KeyFile initFile = Glib::KeyFile();
            initFile.load_from_file(filename,Glib::KEY_FILE_KEEP_TRANSLATIONS);
            initFile.set_boolean(group,key,value);
            initFile.save_to_file(filename);
        }
    }
    
    //DBUS Code - use glib::init() before using them.
    namespace Dbus{
        bool isValidScheme(Glib::ustring scheme){
            Glib::ustring file = scheme;
            
            //change if not equal in name.
            if (scheme.compare("org.cinnamon.theme") == 0 || scheme.compare("org.cinnamon.background") == 0 )   
                file = "org.cinnamon";
            
            file = "/usr/share/glib-2.0/schemas/" + file + ".gschema.xml";
            return (Glib::file_test(file,Glib::FILE_TEST_EXISTS));
        }
        Glib::ustring CreateDbusFile(){
            if (!Glib::file_test("/etc/dconf/db/local.d/emilia",Glib::FILE_TEST_EXISTS)){
                Gio::File::create_for_path("/etc/dconf/db/local.d/emilia")->create_file(Gio::FILE_CREATE_NONE);
            }
            return "/etc/dconf/db/local.d/emilia";
        }
        //--------
        Glib::ustring GetString (Glib::ustring scheme, Glib::ustring key){
            if (isRoot()){
                return Init::GetString(CreateDbusFile(), 
                                Glib::Regex::create("[.]")->replace(scheme,0,"/",Glib::REGEX_MATCH_NOTEOL), 
                                key);
            }
            if (!isValidScheme(scheme)) throw InvalidDBusCode(scheme.c_str());
            Glib::RefPtr<Gio::Settings> dBusInstance = Gio::Settings::create(scheme);
            return dBusInstance->get_string(key);
        }
        int GetInt (Glib::ustring scheme, Glib::ustring key){
            if (isRoot()){
                return Init::GetInt(CreateDbusFile(), 
                                Glib::Regex::create("[.]")->replace(scheme,0,"/",Glib::REGEX_MATCH_NOTEOL), 
                                key);
            }
            if (!isValidScheme(scheme)) throw InvalidDBusCode(scheme.c_str());
            Glib::RefPtr<Gio::Settings> dBusInstance = Gio::Settings::create(scheme);
            return dBusInstance->get_int(key);
        }
        bool GetBool (Glib::ustring scheme, Glib::ustring key){
            if (isRoot()){
                return Init::GetBool(CreateDbusFile(), 
                                Glib::Regex::create("[.]")->replace(scheme,0,"/",Glib::REGEX_MATCH_NOTEOL), 
                                key);
            }
            if (!isValidScheme(scheme)) throw InvalidDBusCode(scheme.c_str());
            Glib::RefPtr<Gio::Settings> dBusInstance = Gio::Settings::create(scheme);
            return dBusInstance->get_boolean(key);
        }
        //------
        void SetString (Glib::ustring scheme, Glib::ustring key, Glib::ustring value){
            if (isRoot()){
                return Init::SetString(CreateDbusFile(), 
                                Glib::Regex::create("[.]")->replace(scheme,0,"/",Glib::REGEX_MATCH_NOTEOL), 
                                key, "'" + value + "'");
            }
            if (!isValidScheme(scheme)) throw InvalidDBusCode(scheme.c_str());
            Glib::RefPtr<Gio::Settings> dBusInstance = Gio::Settings::create(scheme);
            dBusInstance->set_string(key,value);
        }
        void SetInt (Glib::ustring scheme, Glib::ustring key, int value){
            if (isRoot()){
                return Init::SetInt(CreateDbusFile(), 
                                Glib::Regex::create("[.]")->replace(scheme,0,"/",Glib::REGEX_MATCH_NOTEOL), 
                                key, value);
            }
            if (!isValidScheme(scheme)) throw InvalidDBusCode(scheme.c_str());
            Glib::RefPtr<Gio::Settings> dBusInstance = Gio::Settings::create(scheme);
            dBusInstance->set_int(key,value);
        }
        void SetBool (Glib::ustring scheme, Glib::ustring key, bool value){
            if (isRoot()){
                return Init::SetBool(CreateDbusFile(), 
                                Glib::Regex::create("[.]")->replace(scheme,0,"/",Glib::REGEX_MATCH_NOTEOL), 
                                key, value);
            }
            if (!isValidScheme(scheme)) throw InvalidDBusCode(scheme.c_str());
            Glib::RefPtr<Gio::Settings> dBusInstance = Gio::Settings::create(scheme);
            dBusInstance->set_boolean(key,value);
        }
    }

    //XML code
    namespace Xml {
        void SetXml(Glib::ustring file_path, Glib::ustring xPath, Glib::ustring value){
            xmlpp::DomParser parser;
            try{
                parser.parse_file(file_path);
                
                xmlpp::ContentNode* themePtr = (xmlpp::ContentNode*)(
                    parser.get_document()->get_root_node() //Get the root node from the document.
                    ->find(xPath).front() //navigate using xpath and pick the first (only) result
                    ->get_children().front()); //pick the value itself for changing
                
                themePtr->set_content(value);
                parser.get_document()->write_to_file_formatted(file_path);
            }
            catch(...){
                throw InvalidXml(file_path.c_str());
            }
        }
        Glib::ustring GetXml(Glib::ustring file_path, Glib::ustring xPath){
            xmlpp::DomParser parser;
            try{
                parser.parse_file(file_path);
                
                xmlpp::ContentNode* themePtr = (xmlpp::ContentNode*)(
                    parser.get_document()->get_root_node() //Get the root node from the document.
                    ->find(xPath).front() //navigate using xpath and pick the first (only) result
                    ->get_children().front()); //pick the value itself for changing
                
                Glib::ustring ret = themePtr->get_content();
                parser.get_document()->write_to_file_formatted(file_path);
                return ret;
            }
            catch(...){
                throw InvalidXml(file_path.c_str());
            }
            return "";
        }
        
    }

    //CREATE CONFIGS IF DON'T EXIST
    namespace Create {
        namespace {
            Glib::ustring ConfigCreationChecking(Glib::ustring global_path, Glib::ustring user_path, Glib::ustring file){
                std::vector<Glib::ustring> default_files_dir = {
                    Glib::get_current_dir() + "/DefaultFiles/",
                    Glib::get_user_config_dir() + "/emilia/garden/DefaultFiles/",
                    Glib::get_home_dir() + "/.local/share/emilia/garden/DefaultFiles/",
                    "/usr/local/share/emilia/garden/DefaultFiles/",
                    "/usr/share/emilia/garden/DefaultFiles/"
                };
                
                if (isRoot()) {
                    for (Glib::ustring dir : default_files_dir){
                        if (!Glib::file_test(dir + file, Glib::FILE_TEST_EXISTS)) continue;
                        if (!Glib::file_test(global_path + file, Glib::FILE_TEST_EXISTS))
                            FileCreate(dir, global_path, file);
                        return global_path + file;
                    }
                    throw NoDefaultFileException(file.c_str());
                }
                else  {
                    for (Glib::ustring dir : default_files_dir){
                        if (!Glib::file_test(dir + file, Glib::FILE_TEST_EXISTS)) continue;
                        if (!Glib::file_test(user_path + file, Glib::FILE_TEST_EXISTS))
                            FileCreate(dir, user_path, file);
                        return user_path + file;
                    }
                    throw NoDefaultFileException(file.c_str());
                }
            }
        }
        Glib::ustring Garden(){
            return ConfigCreationChecking("/etc/emilia/", Glib::get_user_config_dir() + "/emilia/","garden.ini");
        }
        Glib::ustring Gtk2(){
            return ConfigCreationChecking("/etc/skel/.config/", Glib::get_user_config_dir() + "/",".gtkrc-2.0");
        }
        Glib::ustring Gtk3(){
            return ConfigCreationChecking("/etc/skel/.config/gtk-3.0/", Glib::get_user_config_dir() + "/gtk-3.0/","settings.ini");
        }
        Glib::ustring Qtct(){
            return ConfigCreationChecking("/etc/skel/.config/qt5ct/", Glib::get_user_config_dir() + "/qt5ct/","qt5ct.conf");
        }
        Glib::ustring Kvantum(){
            return ConfigCreationChecking("/etc/skel/.config/Kvantum/", Glib::get_user_config_dir() + "/Kvantum/","kvantum.kvconfig");
        }
        Glib::ustring KdeGlobals(){
            return ConfigCreationChecking("/etc/skel/.config/", Glib::get_user_config_dir() + "/","kdeglobals");
        }
        Glib::ustring KdePlasmarc(){
            return ConfigCreationChecking("/etc/skel/.config/", Glib::get_user_config_dir() + "/","plasmarc");
        }
        Glib::ustring KdeWinrc(){
            return ConfigCreationChecking("/etc/skel/.config/", Glib::get_user_config_dir() + "/","kwinrc");
        }
        Glib::ustring Kcminputrc(){
            return ConfigCreationChecking("/etc/skel/.config/", Glib::get_user_config_dir() + "/", "kcminputrc");
        }
        Glib::ustring XfceXsettings(){
            return ConfigCreationChecking("/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/", Glib::get_user_config_dir() + "/xfce4/xfconf/xfce-perchannel-xml/","xsettings.xml");
        }
        Glib::ustring XfceXfwm(){
            return ConfigCreationChecking("/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/", Glib::get_user_config_dir() + "/xfce4/xfconf/xfce-perchannel-xml/","xfwm4.xml");
        }
        Glib::ustring LxqtConfig(){
            return ConfigCreationChecking("/etc/skel/.config/lxqt/", Glib::get_user_config_dir() + "/lxqt/","lxqt.conf");
        }
        Glib::ustring LxqtSession(){
            return ConfigCreationChecking("/etc/skel/.config/lxqt/", Glib::get_user_config_dir() + "/lxqt/","session.conf");
        }
        Glib::ustring LxqtSettings(){
            return ConfigCreationChecking("/etc/skel/.config/pcmanfm-qt/lxqt/", Glib::get_user_config_dir() + "/pcmanfm-qt/lxqt/","settings.conf");
        }
        Glib::ustring Openbox(bool isLxqt){
            std::string file = (isLxqt)? "lxqt-rc.xml":"rc.xml";
            return ConfigCreationChecking("/etc/skel/.config/openbox/", Glib::get_user_config_dir() + "/openbox/", file);
        }
        Glib::ustring Feh(){
            return ConfigCreationChecking("/etc/skel/", Glib::get_home_dir() + "/",".fehbg");
        }
        Glib::ustring DefaultCursor(){
            return ConfigCreationChecking("/etc/skel/.icons/default/", Glib::get_home_dir() + "/.icons/default/","index.theme");
        }
        Glib::ustring LightdmGtkGreeter(){
            return ConfigCreationChecking("/etc/lightdm/", "", "lightdm-gtk-greeter.conf");
        }
    }
    
    namespace Gtk2 {
        void SetValue(Glib::ustring key, Glib::ustring value){
            Glib::ustring location = Create::Gtk2();

            //if regex match, replace in file. else... add in file
            if(!FileMatch  (location, key + "( *?)=( *?)(\".*?\")").empty()){
                FileReplace(location, key + "( *?)=( *?)(\".*?\")",
                                        key + " = \"" + value + "\"");
            }
            else {
                FileAdd(location, key + " = \"" + value + "\"\n" , true);
            }
        }
        Glib::ustring GetValue(Glib::ustring key){
            Glib::ustring location = Create::Gtk2();
            
            Glib::ustring match = FileMatch(location, key + "( *?)=( *?)(\".*?\")");
            if(!match.empty()){
                return std::vector<Glib::ustring>(Glib::Regex::split_simple("\"",match)).at(1); //0 is key, 1 is value, 2 is empty space after.
            }
            else return "";
        }
    }
    
    Glib::ustring GetGtkFont(bool isGtk2){
        std::vector<Glib::ustring> list;
        if (isGtk2)
            list = Glib::Regex::split_simple(" ", Gtk2::GetValue("gtk-font-name"));
        else 
            list = Glib::Regex::split_simple(" ", Init::GetString(Create::Gtk3(), "Settings", "gtk-font-name"));
            
        if (list.size() < 3) return "";
        Glib::ustring font = "";
        for(int i=0; i<(int)list.size();i++) font += list.at(i);
        
        return font + "," + list.at(list.size()-2) + "," + list.at(list.size()-1);
    }
}

#include "desktop.cpp"
