#include <emilia-garden.h>

namespace EmiliaGarden {
    Glib::ustring Xfce::GetName               (){return "xfce";}
    void          Xfce::SetGtk3Theme          (Glib::ustring theme_name){
        if (!isValidGtk3Theme(theme_name)) throw InvalidTheme();
        Xml::SetXml(Create::XfceXsettings(),"./property[@name='Net']/property[@name='ThemeName']/@value",theme_name);
    }
    Glib::ustring Xfce::GetGtk3Theme          (){
        if(Xml::GetXml(Create::XfceXsettings(),"./property[@name='Net']/property[@name='ThemeName']/@type").compare("empty") != 0){
                return Xml::GetXml(Create::XfceXsettings(),"./property[@name='Net']/property[@name='ThemeName']/@value");
        }
        else return "";
    }
    
    void          Xfce::SetWMTheme            (Glib::ustring theme_name){
        if(!isValidWMTheme(theme_name)) throw InvalidTheme();
        Xml::SetXml(Create::XfceXfwm(),"./property[@name='general']/property[@name='theme']/@value",theme_name);
    }
    Glib::ustring Xfce::GetWMTheme            (){
        return Xml::GetXml(Create::XfceXfwm(),"./property[@name='general']/property[@name='theme']/@value");
    }
    bool          Xfce::isValidWMTheme        (Glib::ustring theme_name){
        //xfwm themes valid locations:
            // ~/.themes
            // /usr/share/themes
        //all of them have xfwm4/themerc. so...

        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.themes/", Glib::get_home_dir() + "/.local/share/themes/", "/usr/share/themes/", "/usr/local/share/themes/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/xfwm4/themerc",Glib::FILE_TEST_EXISTS)){ 
                return true;
            }
        }
        return false;
    }

    void          Xfce::SetDesktopTheme       (Glib::ustring theme_name){}
    Glib::ustring Xfce::GetDesktopTheme       (){return "";}
    bool          Xfce::isValidDesktopTheme   (Glib::ustring theme_name){return true;}
    
    void          Xfce::SetWallpaper          (Glib::ustring wallpaper_path){
        if (!isValidWallpaper(wallpaper_path,false)) throw InvalidTheme();
        //TODO
    }
    Glib::ustring Xfce::GetWallpaper          (){return "";}
    
    void          Xfce::SetIconTheme          (Glib::ustring theme_name){
        Desktop::SetIconTheme(theme_name);
        Xml::SetXml(Create::XfceXsettings(),"./property[@name='Net']/property[@name='IconThemeName']/@value", theme_name);
    }
    Glib::ustring Xfce::GetIconTheme          (){
        return Desktop::GetIconTheme() +
                ". Xfce Gtk3:" + ((Xml::GetXml(Create::XfceXsettings(),"./property[@name='Net']/property[@name='IconThemeName']/@type").compare("empty") != 0) ? 
                                   Xml::GetXml(Create::XfceXsettings(),"./property[@name='Net']/property[@name='IconThemeName']/@value") : "");
    }
        
    void          Xfce::SetCursorTheme        (Glib::ustring theme_name){
        Desktop::SetCursorTheme(theme_name);
        Xml::SetXml(Create::XfceXsettings(),"./property[@name='Gtk']/property[@name='CursorThemeName']/@type", theme_name);
    }
    Glib::ustring Xfce::GetCursorTheme        (){
        return Desktop::GetCursorTheme() + 
                ". Xfce Gtk3:" + ((Xml::GetXml(Create::XfceXsettings(),"./property[@name='Gtk']/property[@name='CursorThemeName']/@type").compare("empty") != 0) ? 
                                   Xml::GetXml(Create::XfceXsettings(),"./property[@name='Gtk']/property[@name='CursorThemeName']/@value") : "");
    }
    
    //void          Xfce::SetFont               (Glib::ustring font_name){}
    //Glib::ustring Xfce::GetFont               (){return "";}
} 
