#include <emilia-garden.h>

namespace EmiliaGarden {
    Glib::ustring Cinnamon::GetName               (){return "cinnamon";}
    void          Cinnamon::SetGtk3Theme          (Glib::ustring theme_name){
        if (!isValidGtk3Theme(theme_name)) throw InvalidTheme();
        Dbus::SetString("org.cinnamon.desktop.interface","gtk-theme",theme_name);
    }
    Glib::ustring Cinnamon::GetGtk3Theme          (){
        return Dbus::GetString("org.cinnamon.desktop.interface","gtk-theme");
    }
    
    void          Cinnamon::SetWMTheme            (Glib::ustring theme_name){
        Dbus::SetString("org.cinnamon.desktop.wm.preferences","theme",theme_name);
    }
    Glib::ustring Cinnamon::GetWMTheme            (){
        return Dbus::GetString("org.cinnamon.desktop.wm.preferences","theme");
    }
    bool          Cinnamon::isValidWMTheme        (Glib::ustring theme_name){return true;}

    void          Cinnamon::SetDesktopTheme       (Glib::ustring theme_name){
        Dbus::SetString("org.cinnamon.theme","name",theme_name);
    }
    Glib::ustring Cinnamon::GetDesktopTheme       (){
        return Dbus::GetString("org.cinnamon.theme","name");
    }
    bool          Cinnamon::isValidDesktopTheme   (Glib::ustring theme_name){
        //cinnamon desktop themes valid locations:
            // ~/.themes
            // /usr/share/themes
        //has to have a cinnamon folder.
        
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.themes/", "/usr/share/themes/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/cinnamon",Glib::FILE_TEST_IS_DIR)){ 
                return true;
            }
        }
        return false;
    }
    
    void          Cinnamon::SetWallpaper          (Glib::ustring wallpaper_path){
        if(!isValidWallpaper(wallpaper_path,true)) throw InvalidTheme();
        Dbus::SetString("org.cinnamon.desktop.background", "picture-uri", checkPathProtocol(wallpaper_path));
    }
    Glib::ustring Cinnamon::GetWallpaper          (){
        return Dbus::GetString("org.cinnamon.desktop.background","picture-uri");
    }
    
    void          Cinnamon::SetIconTheme          (Glib::ustring theme_name){
        Desktop::SetIconTheme(theme_name);
        Dbus::SetString("org.cinnamon.desktop.interface","icon-theme",theme_name);
    }
    Glib::ustring Cinnamon::GetIconTheme          (){
        return Desktop::GetIconTheme() + ". Cinnamon Gtk3:" + Dbus::GetString("org.cinnamon.desktop.interface","icon-theme");
    }

    void          Cinnamon::SetCursorTheme        (Glib::ustring theme_name){
        Desktop::SetCursorTheme(theme_name);
        Dbus::SetString("org.cinnamon.desktop.interface","cursor-theme",theme_name);
    }
    Glib::ustring Cinnamon::GetCursorTheme        (){
        return Desktop::GetCursorTheme() + ". Cinnamon Gtk3:" + Dbus::GetString("org.cinnamon.desktop.interface","cursor-theme");
    }

    //void          Cinnamon::SetFont               (Glib::ustring font_name){}
    //Glib::ustring Cinnamon::GetFont               (){return "";}
} 
