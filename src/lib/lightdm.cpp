#include <emilia-garden.h>

namespace EmiliaGarden {
    Glib::ustring Lightdm::GetName               (){return "lightdm";}
    
    void          Lightdm::SetGtk3Theme          (Glib::ustring theme_name){
        if (!isValidGtk3Theme(theme_name)) throw InvalidTheme();
        if (!isRoot()) throw NotRootException();
        Init::SetString(Create::LightdmGtkGreeter(),"greeter","theme-name",theme_name);
    }
    Glib::ustring Lightdm::GetGtk3Theme          (){
        return Init::GetString(Create::LightdmGtkGreeter(),"greeter","theme-name");
    }
    
    void          Lightdm::SetWallpaper          (Glib::ustring wallpaper_path){
        if (!isValidWallpaper(wallpaper_path,false)) throw InvalidTheme();
        if (!isRoot()) throw NotRootException();
        Init::SetString(Create::LightdmGtkGreeter(),"greeter","background",wallpaper_path);
    }
    Glib::ustring Lightdm::GetWallpaper          (){
        return Init::GetString(Create::LightdmGtkGreeter(),"greeter","background");
    }
    
    void          Lightdm::SetIconTheme          (Glib::ustring theme_name){
        if (!isValidIconTheme(theme_name)) throw InvalidTheme();
        if (!isRoot()) throw NotRootException();
        Init::SetString(Create::LightdmGtkGreeter(),"greeter","icon-theme-name",theme_name);
    }
    Glib::ustring Lightdm::GetIconTheme          (){
        return Init::GetString(Create::LightdmGtkGreeter(),"greeter","icon-theme-name");
    }
        
    void          Lightdm::SetCursorTheme        (Glib::ustring theme_name){
        if (!isValidCursorTheme(theme_name)) throw InvalidTheme();
        if (!isRoot()) throw NotRootException();
        Init::SetString(Create::LightdmGtkGreeter(),"greeter","cursor-theme-name",theme_name);
    }
    Glib::ustring Lightdm::GetCursorTheme        (){
        return Init::GetString(Create::LightdmGtkGreeter(),"greeter","cursor-theme-name");
    }
    
    void          Lightdm::SetFont               (Glib::ustring font_name, int size, Glib::ustring style){
        //ignore style...
        if (!isValidFont(font_name)) throw InvalidTheme();
        if (!isRoot()) throw NotRootException();
        Init::SetString(Create::LightdmGtkGreeter(),"greeter","font-name",font_name + " " + std::to_string(size));
    }
    Glib::ustring Lightdm::GetFont               (){
        //TODO - separate font name from size.
        return Init::GetString(Create::LightdmGtkGreeter(),"greeter","font-name");
    }
    
    
    //=================================================================
    //override so users can't use them on this class.
    void          Lightdm::SetGtk2Theme          (Glib::ustring theme_name){}
    Glib::ustring Lightdm::GetGtk2Theme          (){return "";}       
    
    void          Lightdm::SetGtk3Dark           (bool isDark){}
    bool          Lightdm::GetGtk3Dark           (){return false;}
        
    void          Lightdm::SetQt5Theme           (Glib::ustring theme_name){}
    Glib::ustring Lightdm::GetQt5Theme           (){return "";}
} 
