#include <emilia-garden.h>

namespace EmiliaGarden {
    Glib::ustring Mate::GetName               (){return "mate";}
    void          Mate::SetGtk3Theme          (Glib::ustring theme_name){
        if (!isValidGtk3Theme(theme_name)) throw InvalidTheme();
        Dbus::SetString("org.mate.interface","gtk-theme",theme_name);
    }
    Glib::ustring Mate::GetGtk3Theme          (){
        return Dbus::GetString("org.mate.interface","gtk-theme");
    }
    
    void          Mate::SetWMTheme            (Glib::ustring theme_name){
        SetGtk3Theme(theme_name);
    }
    Glib::ustring Mate::GetWMTheme            (){
        return GetGtk3Theme();
    }
    bool          Mate::isValidWMTheme        (Glib::ustring theme_name){
        return isValidGtk3Theme(theme_name);
    }

    void          Mate::SetDesktopTheme       (Glib::ustring theme_name){
        SetGtk3Theme(theme_name);
    }
    Glib::ustring Mate::GetDesktopTheme       (){
        return GetGtk3Theme();
    }
    bool          Mate::isValidDesktopTheme   (Glib::ustring theme_name){
        return isValidGtk3Theme(theme_name);
    }
    
    void          Mate::SetWallpaper          (Glib::ustring wallpaper_path){
        if(!isValidWallpaper(wallpaper_path,true)) throw InvalidTheme();
        Dbus::SetString("org.mate.background","picture-filename",wallpaper_path);
    }
    Glib::ustring Mate::GetWallpaper          (){
        return Dbus::GetString("org.mate.background","picture-filename");
    }
    
    void          Mate::SetIconTheme          (Glib::ustring theme_name){
        Desktop::SetIconTheme(theme_name);
        Dbus::SetString("org.mate.interface","icon-theme",theme_name);
    }
    Glib::ustring Mate::GetIconTheme          (){
        return Desktop::GetIconTheme() + ". Mate Gtk3:" + Dbus::GetString("org.mate.interface","icon-theme");
    }
        
    void          Mate::SetCursorTheme        (Glib::ustring theme_name){
        Desktop::SetCursorTheme(theme_name);
        Dbus::SetString("org.mate.peripherals-mouse","cursor-theme",theme_name);
    }
    Glib::ustring Mate::GetCursorTheme        (){
        return Desktop::GetCursorTheme() + ". Mate Gtk3:" + Dbus::GetString("org.mate.peripherals-mouse","cursor-theme");
    }
    
    //void          Mate::SetFont               (Glib::ustring font_name){}
    //Glib::ustring Mate::GetFont               (){return "";}
} 
