#include <emilia-garden.h>

namespace EmiliaGarden {
    Glib::ustring Plasma::GetName               (){return "plasma";}
    void          Plasma::SetQt5Theme           (Glib::ustring theme_name){
        if (isValidQt5Widget(theme_name,false)){
            Glib::ustring kdeGlobal = Create::KdeGlobals();
            Init::SetString(kdeGlobal,"KDE","widgetStyle",theme_name.lowercase());
            Init::SetString(kdeGlobal,"General","widgetStyle",theme_name.lowercase());
        }
        else if (isValidKvantumTheme(theme_name)){
            if(!isKvantumInstalled()){
                throw NotInstalledProgram("/usr/lib/qt/plugins/styles/libkvantum.so");
            }
            SetQt5Theme("kvantum");
            Init::SetString(Create::Kvantum(),"General","theme",theme_name);
        }
        else throw InvalidTheme();
    }
    Glib::ustring Plasma::GetQt5Theme           (){
        
        Glib::ustring sty = Init::GetString(Create::KdeGlobals(),"KDE","widgetStyle");
        if(sty.lowercase().compare("kvantum") != 0) return sty;
        else return Init::GetString(Create::Kvantum(),"General","theme");
    }
    
    void          Plasma::SetWMTheme            (Glib::ustring theme_name){
        if(!isValidWMTheme(theme_name)) throw InvalidTheme();
        Init::SetString(Create::KdeWinrc(), "org.kde.kdecoration2", "theme", "__aurorae__svg__" + theme_name);
    }
    Glib::ustring Plasma::GetWMTheme            (){
        return Init::GetString(Create::KdeWinrc(), "org.kde.kdecoration2", "theme").substr(16);
    }
    bool          Plasma::isValidWMTheme        (Glib::ustring theme_name){
        //The themes have to be installed on the aurorae directory, for now, only in /usr/share/aurorae/themes
            //everyone has /themes/NAME/NAMErc
        
        std::vector<Glib::ustring> validLocations = {"/usr/share/aurorae/themes/"};
        for (Glib::ustring local : validLocations) {
            if(Glib::file_test(local + theme_name + "/" + theme_name + "rc",Glib::FILE_TEST_EXISTS)){ 
                return true;
            }
        }
        return false;
    }

    void          Plasma::SetDesktopTheme       (Glib::ustring theme_name){
        if(!isValidDesktopTheme(theme_name)) throw InvalidTheme();
        Init::SetString(Create::KdePlasmarc(),"Theme","name",theme_name);
    }
    Glib::ustring Plasma::GetDesktopTheme       (){
        return Init::GetString(Create::KdePlasmarc(),"Theme","name");
    }
    bool          Plasma::isValidDesktopTheme   (Glib::ustring theme_name){return true;}
    
    void          Plasma::SetWallpaper          (Glib::ustring wallpaper_path){}
    Glib::ustring Plasma::GetWallpaper          (){return "";}
    
    void          Plasma::SetIconTheme          (Glib::ustring theme_name){
        Desktop::SetIconTheme(theme_name);
        Init::SetString(Create::KdeGlobals(), "Icons", "Theme", theme_name);
    }
    Glib::ustring Plasma::GetIconTheme          (){
        return Desktop::GetIconTheme() + ". Plasma Qt5:" + Init::GetString(Create::KdeGlobals(), "Icons", "Theme");
    }
        
    void          Plasma::SetCursorTheme        (Glib::ustring theme_name){
        Desktop::SetCursorTheme(theme_name);
        Init::SetString(Create::Kcminputrc(), "Mouse","cursorTheme", theme_name);        
    }
    Glib::ustring Plasma::GetCursorTheme        (){
        return Desktop::GetCursorTheme() + ". Plasma Qt5:" + Init::GetString(Create::Kcminputrc(), "Mouse","cursorTheme");
    }
    
    //void          Plasma::SetFont               (Glib::ustring font_name){}
    //Glib::ustring Plasma::GetFont               (){return "";}
} 
