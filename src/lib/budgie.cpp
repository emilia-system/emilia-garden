#include <emilia-garden.h>
 
namespace EmiliaGarden {
    Glib::ustring Budgie::GetName               (){return "budgie";}
    void          Budgie::SetGtk3Theme          (Glib::ustring theme_name){
        if (!isValidGtk3Theme(theme_name)) throw InvalidTheme();
        Dbus::SetString("org.gnome.desktop.interface","gtk-theme",theme_name);
    }
    Glib::ustring Budgie::GetGtk3Theme          (){
        return Dbus::GetString("org.gnome.desktop.interface","gtk-theme");
    }
    
    void          Budgie::SetWMTheme            (Glib::ustring theme_name){
        SetDesktopTheme(theme_name); 
    }
    Glib::ustring Budgie::GetWMTheme            (){
        return GetDesktopTheme();
    }
    bool          Budgie::isValidWMTheme        (Glib::ustring theme_name){
        return isValidDesktopTheme(theme_name);
    }

    void          Budgie::SetDesktopTheme       (Glib::ustring theme_name){
        if (!isValidDesktopTheme(theme_name)) throw InvalidTheme();
        Dbus::SetBool("com.solus-project.budgie-panel","builtin-theme",false);
        Dbus::SetBool("com.solus-project.budgie-panel","dark-theme",false); //move it to gtk3-dark...?
        Dbus::SetString("org.gnome.shell.extensions.user-theme","name",theme_name);
    }
    Glib::ustring Budgie::GetDesktopTheme       (){
        return Dbus::GetString("org.gnome.shell.extensions.user-theme","name");
    }
    bool          Budgie::isValidDesktopTheme   (Glib::ustring theme_name){
        //gnome-shell themes valid locations:
            // ~/.themes
            // /usr/share/themes
        //has to have a gnome-shell folder.
        
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.themes/", "/usr/share/themes/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/gnome-shell",Glib::FILE_TEST_IS_DIR)){ 
                return true;
            }
        }
        return false;
    }
    
    void          Budgie::SetWallpaper          (Glib::ustring wallpaper_path){
        if (!isValidWallpaper(wallpaper_path,true)) throw InvalidTheme();
        Dbus::SetString("org.gnome.desktop.background","picture-uri",wallpaper_path);
    }
    Glib::ustring Budgie::GetWallpaper          (){
        return Dbus::GetString("org.gnome.desktop.background","picture-uri");
    }
    
    void          Budgie::SetIconTheme          (Glib::ustring theme_name){
        Desktop::SetIconTheme(theme_name);
        Dbus::SetString("org.gnome.desktop.interface","icon-theme",theme_name);
    }
    Glib::ustring Budgie::GetIconTheme          (){
        return Desktop::GetIconTheme() + ". Budgie Gtk3:" + Dbus::GetString("org.gnome.desktop.interface","icon-theme");
    }
        
    void          Budgie::SetCursorTheme        (Glib::ustring theme_name){
        Desktop::SetCursorTheme(theme_name);
        Dbus::SetString("org.gnome.desktop.interface","cursor-theme",theme_name);
    }
    Glib::ustring Budgie::GetCursorTheme        (){
        return Desktop::GetCursorTheme() + ". Budgie Gtk3:" + Dbus::GetString("org.gnome.desktop.interface","cursor-theme");
    }
    
    //void          Budgie::SetFont               (Glib::ustring font_name){}
    //Glib::ustring Budgie::GetFont               (){return "";}
}
