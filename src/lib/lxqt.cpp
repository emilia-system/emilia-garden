#include <emilia-garden.h>

namespace EmiliaGarden {
    Glib::ustring Lxqt::GetName               (){return "lxqt";}
    void          Lxqt::SetQt5Theme           (Glib::ustring theme_name){
        if (isValidQt5Widget(theme_name,true)){
            Init::SetString(Create::LxqtConfig(),"Qt","style",theme_name);
        }
        else if (isValidKvantumTheme(theme_name)){
            Init::SetString(Create::Kvantum(),"General","theme",theme_name);
        }
        else throw InvalidTheme();
    }
    Glib::ustring Lxqt::GetQt5Theme           (){
        Glib::ustring sty = Init::GetString(Create::LxqtConfig(),"Qt","style");
        if(sty.lowercase().compare("kvantum")) return sty;
        return Init::GetString(Create::Kvantum(),"General","theme");
    }
    
    void          Lxqt::SetWMTheme            (Glib::ustring theme_name){
        if(!isValidWMTheme(theme_name)) throw InvalidTheme();
        Xml::SetXml(Create::Openbox(true), "./*[name()='theme']/*[name()='name']",theme_name);
    }
    Glib::ustring Lxqt::GetWMTheme            (){
        return Xml::GetXml(Create::Openbox(true), "./*[name()='theme']/*[name()='name']");
    }
    bool          Lxqt::isValidWMTheme        (Glib::ustring theme_name){
        //Openbox themes valid locations:
            // ~/.themes
            // /usr/share/themes;
            // they have the file openbox-3/themerc in them
        
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.themes/", Glib::get_home_dir() + "/.local/share/themes/", "/usr/share/themes/", "/usr/local/share/themes/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/openbox-3/themerc",Glib::FILE_TEST_EXISTS)){ 
                return true;
            }
        }
        return false;
    }

    //TODO
    void          Lxqt::SetDesktopTheme       (Glib::ustring theme_name){ }
    Glib::ustring Lxqt::GetDesktopTheme       (){return "";}
    bool          Lxqt::isValidDesktopTheme   (Glib::ustring theme_name){return true;}
    
    void          Lxqt::SetWallpaper          (Glib::ustring wallpaper_path){
        if (!isValidWallpaper(wallpaper_path,false)) throw InvalidTheme();
        Init::SetString(Create::LxqtSettings(), "Desktop", "Wallpaper", wallpaper_path);
    }
    Glib::ustring Lxqt::GetWallpaper          (){
        return Init::GetString(Create::LxqtSettings(), "Desktop", "Wallpaper");
    }
    
    void          Lxqt::SetIconTheme          (Glib::ustring theme_name){
        Desktop::SetIconTheme(theme_name);
        Init::SetString(Create::LxqtConfig(), "General", "icon_theme", theme_name);
    }
    Glib::ustring Lxqt::GetIconTheme          (){
        return Desktop::GetIconTheme() + ". Lxqt Qt5:" + Init::GetString(Create::LxqtConfig(), "General", "icon_theme");
    }

    void          Lxqt::SetCursorTheme        (Glib::ustring theme_name){
        Desktop::SetCursorTheme(theme_name);
        Init::SetString(Create::LxqtConfig(), "Mouse", "cursor_theme", theme_name);
    }
    Glib::ustring Lxqt::GetCursorTheme        (){
        return Desktop::GetCursorTheme() + ". Lxqt Qt5:" + Init::GetString(Create::LxqtConfig(), "Mouse", "cursor_theme");
    }
    
    //void          Lxqt::SetFont               (Glib::ustring font_name){}
    //Glib::ustring Lxqt::GetFont               (){return "";}
}
