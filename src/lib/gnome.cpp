#include <emilia-garden.h>
 
namespace EmiliaGarden {
    Glib::ustring Gnome::GetName               (){return "gnome";}
    void          Gnome::SetGtk3Theme          (Glib::ustring theme_name){
        if (!isValidGtk3Theme(theme_name)) throw InvalidTheme();
        Dbus::SetString("org.gnome.desktop.interface","gtk-theme",theme_name);
    }
    Glib::ustring Gnome::GetGtk3Theme          (){
        return Dbus::GetString("org.gnome.desktop.interface","gtk-theme");
    }
    
    void          Gnome::SetWMTheme            (Glib::ustring theme_name){
        SetDesktopTheme(theme_name); 
    }
    Glib::ustring Gnome::GetWMTheme            (){
        return GetDesktopTheme();
    }
    bool          Gnome::isValidWMTheme        (Glib::ustring theme_name){
        return isValidDesktopTheme(theme_name);
    }

    void          Gnome::SetDesktopTheme       (Glib::ustring theme_name){
        if (!isValidDesktopTheme(theme_name)) throw InvalidTheme();
        Dbus::SetString("org.gnome.shell.extensions.user-theme","name",theme_name);
    }
    Glib::ustring Gnome::GetDesktopTheme       (){
        return Dbus::GetString("org.gnome.shell.extensions.user-theme","name");
    }
    bool          Gnome::isValidDesktopTheme   (Glib::ustring theme_name){
        //gnome-shell themes valid locations:
            // ~/.themes
            // /usr/share/themes
        //has to have a gnome-shell folder.
        
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.themes/", "/usr/share/themes/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/gnome-shell",Glib::FILE_TEST_IS_DIR)){ 
                return true;
            }
        }
        return false;
    }
    
    void          Gnome::SetWallpaper          (Glib::ustring wallpaper_path){
        if (!isValidWallpaper(wallpaper_path,true)) throw InvalidTheme();
        Dbus::SetString("org.gnome.desktop.background","picture-uri",wallpaper_path);
    }
    Glib::ustring Gnome::GetWallpaper          (){
        return Dbus::GetString("org.gnome.desktop.background","picture-uri");
    }
    
    void          Gnome::SetIconTheme          (Glib::ustring theme_name){
        Desktop::SetIconTheme(theme_name);
        Dbus::SetString("org.gnome.desktop.interface","icon-theme",theme_name);
    }
    Glib::ustring Gnome::GetIconTheme          (){
        return Desktop::GetIconTheme() + ". Gnome Gtk3:" + Dbus::GetString("org.gnome.desktop.interface","icon-theme");
    }
        
    void          Gnome::SetCursorTheme        (Glib::ustring theme_name){
        Desktop::SetCursorTheme(theme_name);
        Dbus::SetString("org.gnome.desktop.interface","cursor-theme",theme_name);
    }
    Glib::ustring Gnome::GetCursorTheme        (){
            return  Desktop::GetCursorTheme() + ". Gnome Gtk3:" + Dbus::GetString("org.gnome.desktop.interface","cursor-theme");
    }
    
    //void          Gnome::SetFont               (Glib::ustring font_name){}
    //Glib::ustring Gnome::GetFont               (){return "";}
}
