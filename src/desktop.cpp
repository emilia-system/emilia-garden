#include <emilia-garden.h>
#include <iostream> //system() function. needed for feh.

namespace EmiliaGarden{
        
    Glib::ustring Desktop::GetName               (){return "generic";}

    /// @brief Generic set function for Gtk2
    /// 
    /// It runs isValidGtk2Theme() before trying to set the theme.
    ///
    /// If the program is being executed as root, it alters the /etc/skel/.gtkrc-2.0 file,
    /// else it alters the ~/.gtkrc-2.0 in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    ///
    /// @param theme_name The name of the gtk2 theme, without the directory, as the folder name. 
    /// @throws InvalidTheme
    /// @sa isValidGtk2Theme()
    void          Desktop::SetGtk2Theme          (Glib::ustring theme_name){
        Glib::ustring location;
        if (!isValidGtk2Theme(theme_name, &location)) throw InvalidTheme();
        //everyone is the same. so the objective is to be universal.
        
        Glib::ustring configFile = Create::Gtk2();
        Gtk2::SetValue("gtk-theme-name",theme_name);
        
        //include part
        if(!FileMatch(configFile, "include( +?)(\".*?\")").empty()){
            FileReplace(configFile,
                    "include( +?)(\".*?\")",
                    "include \"" + location + "/" + theme_name + "/gtk-2.0/gtkrc\"");
        }
        else {
            FileAdd(configFile, "include \"" + location + "/" + theme_name + "/gtk-2.0/gtkrc\"\n", true);
        }
    }

    /// @brief Generic get function for Gtk2
    /// 
    /// If the program is being executed as root, it checks the /etc/skel/.gtkrc-2.0 file. Else it alters the ~/.gtkrc-2.0 in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    /// 
    /// @return The name of the gtk2 theme that is being used.
    Glib::ustring Desktop::GetGtk2Theme          (){
        return Gtk2::GetValue("gtk-theme-name");
    }

    /// @brief Checks to see if the gtk2 theme passed is valid
    ///
    /// Used only in SetGtk2Theme(), but it's exposed to the library anyway.
    ///
    /// This method checks if the folder in a valid place contains a gtk2 folder inside the directory in a valid location.
    /// @param theme_name The name of the theme to be tested, the same way as you would in SetGtk2Theme();
    /// @param location Gtk2 is special. SetGtk2Theme() also needs the directory where the theme is, so location alters inside the function to the new directory. Pass a empty string or a not used one.
    ///
    /// @return The theme is valid or not.
    bool          Desktop::isValidGtk2Theme      (Glib::ustring theme_name, Glib::ustring* location){
        //gtk2 themes valid locations:
            // ~/.themes
            // ~/.local/share/themes
            // /usr/local/share/themes
            // /usr/share/themes
        //all of them have gtkrc. so...
        
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.themes/", Glib::get_home_dir() + "/.local/share/themes/", "/usr/local/share/themes/", "/usr/share/themes/"};
        for (Glib::ustring local : validLocations) {
            if(Glib::file_test(local + theme_name + "/gtk-2.0/gtkrc",Glib::FILE_TEST_EXISTS)){ 
                *location =    local + theme_name + "/gtk-2.0/gtkrc" ;
                return true;
            }
        }
        return false;
    }
    //----------------------------------------------------------------------------------
        
    /// @brief Generic set function for Gtk3
    /// 
    /// It runs isValidGtk3Theme() before trying to set the theme.
    ///
    /// If the program is being executed as root, it alters the /etc/skel/.config/gtk-3.0/settings.ini file,
    /// else it alters the ~/.config/gtk-3.0/settings.ini in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    ///
    /// @param theme_name The name of the gtk3 theme without the directory, as the folder name. 
    /// @throws InvalidTheme
    /// @sa isValidGtk3Theme()
    void          Desktop::SetGtk3Theme          (Glib::ustring theme_name){
        if (!isValidGtk3Theme(theme_name)) throw InvalidTheme();
        Glib::ustring location = Create::Gtk3();
        Init::SetString(location, "Settings","gtk-theme-name",theme_name);
    }

    /// @brief Generic get function for Gtk3
    /// 
    /// If the program is being executed as root, it alters the /etc/skel/.config/gtk-3.0/settings.ini file,
    /// else it alters the ~/.config/gtk-3.0/settings.ini in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    /// 
    /// @param theme_name The name of the gtk3 theme without the directory, as the folder name.
    Glib::ustring Desktop::GetGtk3Theme          (){
        Glib::ustring location = Create::Gtk3();
        return Init::GetString(location, "Settings", "gtk-theme-name");
    }

    /// @brief Checks to see if the gtk3 theme passed is valid
    ///
    /// Used only in SetGtk3Theme(), but it's exposed to the library anyway.
    ///
    /// This method checks if the folder in a valid place contains a valid index.theme.
    ///
    /// @param theme_name The name of the theme to be tested, the same way as you would in SetGtk3Theme();
    /// @return The theme is valid or not.
    bool          Desktop::isValidGtk3Theme      (Glib::ustring theme_name) {
        //gtk3 themes valid locations:
            // ~/.themes
            // ~/.local/share/themes
            // /usr/local/share/themes
            // /usr/share/themes
            
        Glib::ustring location;
        bool isValid = false;
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.themes/", Glib::get_home_dir() + "/.local/share/themes/", "/usr/local/share/themes/", "/usr/share/themes/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/index.theme", Glib::FILE_TEST_EXISTS)){ 
                location =     local + theme_name + "/index.theme" ;
                isValid = true;
            }
        }
        if(!isValid) return false;
        
        //location found, begin validation...
        Glib::KeyFile initFile = Glib::KeyFile();
        initFile.load_from_file(location, Glib::KEY_FILE_KEEP_TRANSLATIONS);
        
        if (!initFile.get_string("X-GNOME-Metatheme","GtkTheme").empty()) return true;
        else return false;

    }

    //----------------------------------------------------------------------------------
        
    /// @brief Generic set function for Gtk3 Dark Preferences
    /// 
    /// If the program is being executed as root, it alters the /etc/skel/.config/gtk-3.0/settings.ini file,
    /// else it alters the ~/.config/gtk-3.0/settings.ini in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    ///
    /// There is no check function, as it only has 2 possible values anyway, and both are valid inputs.
    /// 
    /// @param isDark Boolean saying if gtk3 should prefer dark theme or not.
    void          Desktop::SetGtk3Dark           (bool isDark) {
        Glib::ustring location = Create::Gtk3();            
        Init::SetBool(location, "Settings", "gtk-application-prefer-dark-theme", isDark);
    }

    /// @brief Generic get function for Gtk3 Dark Preferences
    /// 
    /// If the program is being executed as root, it alters the /etc/skel/.config/gtk-3.0/settings.ini file,
    /// else it alters the ~/.config/gtk-3.0/settings.ini in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    /// 
    /// @return If the dark preferences are being used or not.
    bool          Desktop::GetGtk3Dark           (){
        Glib::ustring location = Create::Gtk3();
        return Init::GetBool(location, "Settings", "gtk-application-prefer-dark-theme");
    }
    //----------------------------------------------------------------------------------
        
    /// @brief Generic set function for Qt5 theme
    /// 
    /// The generic implementation relies on the qt5ct being installed. On login, this program
    /// checks to see if the running desktop is qt-based or not. If it is, the generic only 
    /// makes it worse, so the enviroment variable QT_QPA_PLATFORM is set to qt5ct only in non-qt desktops.
    ///
    /// This means that the generic set only works for non-qt desktops. 
    /// For qt desktops, overrite this function in subclass.
    /// 
    /// This function tries to set a qt style with the parameter. 
    /// If the program is being executed as root, it alters the /etc/skel/.config/qt5ct/qt5ct.conf file,
    /// else it alters the ~/.config/qt5ct/qt5ct.conf in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    ///
    /// If it's not a valid Qt style, it tries to set the kvantum theme with the parameter.
    /// If it is a kvantum theme, the qt style is set to kvantum. 
    ///
    /// If the program is being executed as root, it alters the /etc/skel/.config/Kvantum/kvantum.kvconfig file,
    /// else it alters the ~/.config/Kvantum/kvantum.kvconfig in your home.
    /// If the file don't exist, the call in Create:: also creates it.
    /// 
    /// @param theme_name A style name or a kvantum theme name.
    /// @throws NotInstalledProgram qt5ct, or kvantum if theme is a kvantum theme.
    /// @throws InvalidTheme if neither styles or kvantum have that theme.
    /// @sa isValidQt5Widget() isValidKvantumTheme() 
    void          Desktop::SetQt5Theme           (Glib::ustring theme_name){
        //WARNING, THIS DEPENDS ON THE QT_QPA_PLATFORMTHEME ENVIROMENT VARIABLE. ON THE GARDEN INSTALLATION, IT WAS ADDED TO /ETC/PROFILE.
        //USED ON EVERYONE, EXCEPT THE ONES THAT USES QT AS MAIN TOOLKIT (KDE AND LXQT)
        if (!isQt5ctInstalled()){
            throw NotInstalledProgram("/usr/bin/qt5ct");
        }
        
        if(isValidQt5Widget(theme_name,false)){
            //replace the current widget for the one desired.
            //qtct5 is defined already in /etc/profile by the installation for non-qt desktops.
            
            Glib::ustring location = Create::Qtct();
            Init::SetString(location, "Appearance", "style", theme_name);

        }
        else if (isValidKvantumTheme(theme_name)){
            if(!isKvantumInstalled()){
                throw NotInstalledProgram("/usr/lib/qt/plugins/styles/libkvantum.so");
            }
            //change the widget for kvantum and change configuration.
            SetQt5Theme("kvantum");
            Glib::ustring location = Create::Kvantum();
            
            Init::SetString(location,"General","theme",theme_name);
        }
        else throw InvalidTheme();
    }

    /// @brief Generic get function for Qt5 theme
    /// 
    /// Get the theme in qt5ct. see SetQt5Theme() for more details.
    /// 
    /// @return Qt5ct style, or the kvantum theme if style == kvantum.
    /// @sa SetQt5Theme()
    Glib::ustring Desktop::GetQt5Theme           (){
        Glib::ustring location = Create::Qtct();
        Glib::ustring sty = Init::GetString(location ,"Appearance", "style");

        if (sty.compare("kvantum") != 0) return sty;
        //is kvantum...
        return Init::GetString(Create::Kvantum(),"General","theme");
    }

    /// @brief Checks to see if the Qt5 style passed is valid
    ///
    /// Used only in SetGtk3Theme(), but it's exposed to the library anyway.
    ///
    /// This method has a rudimentary check, that could be improved. it checks common names like 
    /// fusion or windows, and if it fails, it tries to check the qt styles folder in 
    /// /usr/lib/qt/plugins/styles/
    ///
    /// @param theme The name of the style to be tested, the same way as you would in SetQt5Theme(). Its called theme for consistency.
    /// @param isLxqt Lxqt has a diferent naming to the styles, varying in (upper)case.
    /// @return The style is valid or not.
    bool          Desktop::isValidQt5Widget      (Glib::ustring theme, bool isLxqt) {
        //test if is valid widget.
        //comes with qt:
            //fusion & windows
        //rest has to pass to a lib test.
        //lxqt is key sensitive for fusion, windows, and oxygen. oxygen have a .so with his name, he would pass the second test and not really apply the theme. it's a BUG
        
        if(isLxqt){
            //FIXME
            if   (theme.compare("Fusion") == 0 || theme.compare("Windows") == 0 || theme.compare("kvantum") == 0 || theme.compare("Oxygen") == 0)
                return true;
        }
        else if (theme.lowercase().compare("fusion") == 0 || theme.lowercase().compare("windows") == 0 || theme.lowercase().compare("kvantum") == 0 ){
            return true;
        }
        else if ((Glib::file_test("/usr/lib/qt/plugins/styles/lib" + theme + ".so",Glib::FILE_TEST_EXISTS) || Glib::file_test("/usr/lib/qt/plugins/styles/" + theme + ".so",Glib::FILE_TEST_EXISTS)) &&
            theme.compare("libkvantum") != 0){ 
            //test in the styles folder. But libkvantum.so is just kvantum, so it's not valid as a style!
            return true;
        }
        return false;
    }

    /// @brief Checks to see if the kvantum theme passed is valid
    ///
    /// Used only in SetQt5Theme(), but it's exposed to the library anyway.
    ///
    /// This method checks for "theme".kvconfig file in the theme folder in a valid location.
    ///
    /// @param theme The name of the theme to be tested, the same way as you would in SetQt5Theme().
    /// @param isLxqt Lxqt has a diferent naming to the styles, varying in (upper)case.
    /// @return The theme is valid or not.
    bool          Desktop::isValidKvantumTheme   (Glib::ustring theme) {
        // /usr/share/Kvantum/ is the theme folder.
        //so test for the theme/theme.kvconfig
        
        if(Glib::file_test("/usr/share/Kvantum/" + theme + "/" + theme + ".kvconfig", Glib::FILE_TEST_EXISTS) || Glib::file_test(Glib::get_home_dir() + "/.local/share/Kvantum/" + theme + "/" + theme + ".kvconfig", Glib::FILE_TEST_EXISTS))
            return true;
        return false;
    }
    //----------------------------------------------------------------------------------
        
    /// @brief Generic Set window manager
    ///
    /// This function is empty. 
    ///
    /// There is no generic windowing, so there is no generic to be setted.
    ///
    /// @param theme The name of the theme to be setted.
    void          Desktop::SetWMTheme            (Glib::ustring theme_name){}

    /// @brief Generic Get window manager
    ///
    /// This function is empty. 
    ///
    /// There is no generic windowing, so there is no generic to be getted.
    ///
    /// @return a empty string.
    Glib::ustring Desktop::GetWMTheme            (){return "";}

    /// @brief Generic Check of the window manager theme.
    ///
    /// This function is empty. 
    ///
    /// There is no generic windowing, so there is no generic to be checked.
    ///
    /// @return on generic, always false.
    bool          Desktop::isValidWMTheme        (Glib::ustring theme_name){return false;}

    //----------------------------------------------------------------------------------
        
    /// @brief Generic Set of the desktop theme.
    ///
    /// This function is empty.
    ///
    /// There is no generic desktop, so there is no generic to be setted.
    ///
    /// @param theme_name on generic, this is useless. It's here to be the format for childs.
    void          Desktop::SetDesktopTheme       (Glib::ustring theme_name){}

    /// @brief Generic Get of the desktop theme.
    ///
    /// This function is empty.
    ///
    /// There is no generic desktop, so there is no generic to be getted.
    ///
    /// @return on generic, returns a empty string. It's here to be the format for childs.
    Glib::ustring Desktop::GetDesktopTheme       (){return "";}

    /// @brief Generic Check of the desktop theme.
    ///
    /// This function is empty. 
    ///
    /// There is no generic desktop, so there is no generic to be checked.
    ///
    /// @return on generic, always false.
    bool          Desktop::isValidDesktopTheme   (Glib::ustring theme_name){return false;}

    //----------------------------------------------------------------------------------
        
    /// @brief Generic Set for the wallpaper.
    ///
    /// This function uses the feh command. So you will need it installed if you want to run it!
    ///
    /// @throws NotInstalledProgram feh, if /usr/bin/feh is not installed.
    /// @throws InvalidTheme if the wallpaper is invalid.
    /// @param wallpaper_path The path to the wallpaper.
    void          Desktop::SetWallpaper          (Glib::ustring wallpaper_path){
        if (!Glib::file_test("/usr/bin/feh",Glib::FILE_TEST_EXISTS)){
            throw NotInstalledProgram("/usr/bin/feh");
        }
        else if (!isValidWallpaper(wallpaper_path,false)){
            throw InvalidTheme();
        }
        //TODO for someone's sake. find a way to get into feh's library instead of this.
        if(isRoot()){
            //set old school way in /etc/skel/.fehbg
            FileReplace(Create::Feh(), "(\'.*?\')", wallpaper_path);
        }
        else 
            system(("/usr/bin/feh --bg-scale " + wallpaper_path).c_str());
        //TODO FIXME oh noooo - ~/.fehbg must be ran everytime the x11 starts! so this is just the begining...
    }

    /// @brief Generic Get for the wallpaper.
    ///
    /// The function search for the ~/.fehbg and gets the path from there.
    /// Thanks to the Create::Feh function, it never returns empty, it returns what is in the default file.
    ///
    /// @return wallpaper_path The wallpaper path.
    Glib::ustring Desktop::GetWallpaper          (){
        return std::vector<Glib::ustring>( 
                Glib::Regex::split_simple("'",
                    FileGetContents(Gio::File::create_for_path(Create::Feh())))
            ).at(1);
            //so... we open the file in user home, split using "'", and return the middle term, with is the wallpaper path. that's it.
    }
    
    /// @brief Generic Check of the wallpaper.
    ///
    /// This function check the wallpaper to see if the wallpaper is valid.
    /// 
    /// TODO - Change the way, instead of testing the extension, test the file type.
    ///
    /// @param wallpaper_path The path to the wallpaper.
    /// @param haveXml True if the desktop accepts xml as a wallpaper, like Gnome.
    /// @return True if it's valid, false if it's not valid.
    bool          Desktop::isValidWallpaper      (Glib::ustring wallpaper_path, bool haveXml) {
        //tests the extension. that's it.
        //TODO - use Gio.FileInfo for better checking
        Glib::ustring extension = wallpaper_path.substr( wallpaper_path.find_last_of("."));
        
        if (haveXml && extension.lowercase().compare(".xml") == 0) return true;
        
        if( extension.lowercase().compare(".png") == 0 ||
            extension.lowercase().compare(".jpg") == 0 ||
            extension.lowercase().compare(".jpeg") == 0 ||
            extension.lowercase().compare(".bmp") == 0 ){
            
            return true;
        }
        return false;
    }
    
    //-------------------------------------------------------------------------
    
    /// @brief Generic Set for the icon theme.
    ///
    /// This function is used in all the subclasses also. It changes the configuration
    /// for default Gtk2, Gtk3 and Qtct (as a generic change).
    ///
    /// @throws InvalidTheme if the theme is invalid.
    /// @param theme_name The name of the icon theme without the directory as the folder name.
    void          Desktop::SetIconTheme          (Glib::ustring theme_name){
        if (!isValidIconTheme (theme_name)) throw InvalidTheme();
        Gtk2::SetValue("gtk-icon-theme-name",theme_name);
        Init::SetString(Create::Gtk3(), "Settings", "gtk-icon-theme-name", theme_name);
        Init::SetString(Create::Qtct(), "Appearance", "icon_theme", theme_name);
    }
    
    /// @brief Generic Get for the icon theme.
    ///
    /// This function is used in all the subclasses also. It gets the configuration
    /// setted by SetIconTheme();
    ///
    /// @return A string with the format: Gtk2: (result). Gtk3:(result). Qt5:(result)
    /// @sa SetIconTheme()
    Glib::ustring Desktop::GetIconTheme          (){
        return "Gtk2:" + Gtk2::GetValue("gtk-icon-theme-name")
        + ". Gtk3:" + Init::GetString(Create::Gtk3(), "Settings", "gtk-icon-theme-name")
        + ". Qt5:"  + Init::GetString(Create::Qtct(), "Appearance", "icon_theme");
    }
    
    /// @brief Generic Check of the icon theme.
    ///
    /// This function check the icon theme to see if the theme is valid, testing for the icon-theme.cache first
    /// and the index.theme existence and the absense of the cursors folder.
    /// 
    /// @param theme_name The name of the icon theme without the directory.
    /// @return True if it's valid, false if it's not valid.
    bool          Desktop::isValidIconTheme      (Glib::ustring theme_name){
        //Valid icon themes locations
            // ~/.icons
            // ~/.local/share/icons
            // /usr/local/share/icons
            // /usr/share/icons
            // TODO? - can have icon-theme.cache. so... improvements can be made
        
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.icons/", Glib::get_home_dir() + "/.local/share/icons/", "/usr/local/share/icons/", "/usr/share/icons/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/icon-theme.cache", Glib::FILE_TEST_EXISTS)){ 
                //in usr/share, you have the icon-theme.cache ALWAYS. pacman and package managers make sure that the cache is updated.
                return true;
            }
            else if( Glib::file_test(local + theme_name + "/index.theme", Glib::FILE_TEST_EXISTS) &&
                    !Glib::file_test(local + theme_name + "/cursors",     Glib::FILE_TEST_IS_DIR)){
                return true;
            }
        }
        return false;
        //there is no validation on index.theme for reasons... see HighContrast theme's index.theme for an example.
            //the file is too big/complex for Glib to parse it.
    }
    
    //-------------------------------------------------------------------------
    
    /// @brief Generic Set for the cursor theme
    ///
    /// This function is used in all the subclasses also. It changes the configuration
    /// for default Gtk2, Gtk3 and Qtct (as a generic change). But also changes for the
    /// default in the entire desktop, using a default cursor theme in the .icon folder 
    /// in the home directory. It creates a file with "Inherits" and the theme name.
    ///
    /// @throws InvalidTheme if the theme is invalid.
    /// @param theme_name The name of the cursor theme without the directory as the folder name.
    void          Desktop::SetCursorTheme        (Glib::ustring theme_name){
        if (!isValidCursorTheme(theme_name)) throw InvalidTheme();
        
        Init::SetString(Create::DefaultCursor(), "Icon Theme", "Inherits", theme_name);
        Gtk2::SetValue("gtk-cursor-theme-name",theme_name);
        Init::SetString(Create::Gtk3(), "Settings", "gtk-cursor-theme-name", theme_name);
        //qt5 is same as default for now.
    }
    
    /// @brief Generic Get for the cursor theme.
    ///
    /// This function is used in all the subclasses also. It gets the configuration
    /// setted by SetCursorTheme();
    /// qt5 is the same as the default for now, as the qt5ct do not support cursor yet.
    ///
    /// @return A string with the format: Default: (result).Gtk2: (result). Gtk3:(result). Qt5:(result)
    /// @sa SetCursorTheme()
    Glib::ustring Desktop::GetCursorTheme        (){
        return "Default:" + Init::GetString(Create::DefaultCursor(), "Icon Theme", "Inherits")
            + ". Gtk2:"  + Gtk2::GetValue("gtk-cursor-theme-name") 
            + ". Gtk3:"  + Init::GetString(Create::Gtk3(), "Settings", "gtk-cursor-theme-name")
            + ". Qt5:"   + Init::GetString(Create::DefaultCursor(), "Icon Theme", "Inherits");
            //qt5 is same as default for now.
    }
    
    /// @brief Generic Check of the cursor theme.
    ///
    /// This function check the cursor theme to see if the theme is valid, testing for the cursors folder
    /// and the index.theme existence.
    /// 
    /// @param theme_name The name of the cursor theme without the directory.
    /// @return True if it's valid, false if it's not valid.
    bool          Desktop::isValidCursorTheme    (Glib::ustring theme_name){
        //Cursor themes valid locations:
            // ~/.icons
            // ~/.local/share/icons
            // /usr/local/share/icons
            // /usr/share/icons
        
        bool is_valid = false;
        Glib::ustring location = "";
        std::vector<Glib::ustring> validLocations = {Glib::get_home_dir() + "/.icons/", Glib::get_home_dir() + "/.local/share/icons/", "/usr/local/share/icons/", "/usr/share/icons/"};
        for (Glib::ustring local : validLocations){
            if(Glib::file_test(local + theme_name + "/index.theme", Glib::FILE_TEST_EXISTS) &&
               Glib::file_test(local + theme_name + "/cursors",     Glib::FILE_TEST_IS_DIR)){
                location = local;
                is_valid = true;
                break;
            }
        }
        if (!is_valid) return false;
        
        //location found, begin validation...
        Glib::KeyFile initFile = Glib::KeyFile();
        initFile.load_from_file(location + "/" + theme_name + "/index.theme",Glib::KEY_FILE_KEEP_TRANSLATIONS);
        
        if (!initFile.get_string("Icon Theme","Name").empty()) return true;
        else return false;
    }

    //-------------------------------------------------------------------------
    
    /// @brief Generic Set for the font
    ///
    /// This function is used in all the subclasses also. It changes the configuration
    /// for default Gtk2, Gtk3 and Qtct... well... qtct is kinda hard because we don't
    /// understand what is happening there. It looks like kde also uses this way...?
    ///
    /// We need help with the qt part. also aplies to plasma.
    ///
    /// @throws InvalidTheme if the font is invalid.
    /// @param theme_name The name of the font.
    void          Desktop::SetFont               (Glib::ustring font_name, int size, Glib::ustring style){
        Gtk2::SetValue("gtk-font-name",font_name + " " + style + " " + std::to_string(size));
        Init::SetString(Create::Gtk3(), "Settings", "gtk-font-name", font_name + " " + style + " " + std::to_string(size));
        //TODO qtct5 and wtf is he using?
    }
    
    /// @brief Generic Get for the font.
    ///
    /// This function is used in all the subclasses also. It gets the configuration
    /// setted by SetFont();
    /// qt5 is missing because qt5ct uses a weird format we do not know yet.
    ///
    /// @return A string with the format: Gtk2: (result). Gtk3:(result). Qt5:(result)
    /// @sa SetFont()
    Glib::ustring Desktop::GetFont               (){
        //split and put in order. no spaces in them. only ','s;
        
        return "Gtk2:"   + GetGtkFont(true) 
            + ". Gtk3:" + GetGtkFont(false)
            + ". Qt5:"  + ""; //TODO
    }
    bool          Desktop::isValidFont           (Glib::ustring font_name) {/*????*/ return true;};

}
