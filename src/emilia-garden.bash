#/usr/bin/env bash
_emilia_garden_completions()
{
    if [ "${#COMP_WORDS[@]}" == "2" ]; then
        COMPREPLY=($(compgen -W "get set list-all help reset" -- "${COMP_WORDS[1]}"))
        return
    elif [[ "${COMP_WORDS[1]}" == "help" || "${COMP_WORDS[1]}" == "list-all" ]];then
        return


    elif [ "${#COMP_WORDS[@]}" == "3" ]; then
        COMPREPLY=($(compgen -W "current all generic gnome plasma xfce cinnamon mate lxqt budgie" -- "${COMP_WORDS[2]}"))
        return

    elif [[ "${COMP_WORDS[1]}" == "reset" ]];then
        return
    elif [ "${#COMP_WORDS[@]}" == "4" ]; then
        COMPREPLY=($(compgen -W "gtk2-theme gtk3-theme gtk3-dark qt5-theme wm-theme desktop-theme wallpaper icon-theme cursor-theme font" -- "${COMP_WORDS[3]}"))
        return

    elif ! [ "${COMP_WORDS[1]}" == "set" ]; then
        return;
    elif [ "${#COMP_WORDS[@]}" == "5" ]; then
        #custom logic for each one in #4.
        
        if [ "${COMP_WORDS[3]}" == "gtk2-theme" ]; then
            local list_themes=()
            local valid_locations=()
                       
            for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                if [ -d "$_possible_locations" ]; then
                    valid_locations+=("$_possible_locations")
                fi
            done

            for _valid in "${valid_locations[@]}"; do
                for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                    if [ -f "$_valid/$_dir"/gtk-2.0/gtkrc ]; then
                        list_themes+=("$_dir")
                    fi
                done
            done
            
            COMPREPLY=($(compgen -W "${list_themes[*]}" -- "${COMP_WORDS[4]}"))
            
        elif [ "${COMP_WORDS[3]}" == "gtk3-theme" ]; then
             
            for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                if [ -d "$_possible_locations" ]; then
                    valid_locations+=("$_possible_locations")
                fi
            done

            for _valid in "${valid_locations[@]}"; do
                for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                    if [ -f "$_valid/$_dir"/index.theme ]; then
                        list_themes+=("$_dir")
                    fi
                done
            done
            
            COMPREPLY=($(compgen -W "${list_themes[*]}" -- "${COMP_WORDS[4]}"))
            
        elif [ "${COMP_WORDS[3]}" == "gtk3-dark" ]; then
            COMPREPLY=($(compgen -W "true false" -- "${COMP_WORDS[4]}"))
            
        elif [ "${COMP_WORDS[3]}" == "qt5-theme" ]; then
            
            local list_themes=()
            #styles
            for _dir in $(ls "/usr/lib/qt/plugins/styles/" | sed "s/\n/ /g"); do
                if [ -f "$_valid/$_dir"/index.theme ]; then
                    list_themes+=("$_dir")
                fi
            done
            if [ "${COMP_WORDS[2]}" == "lxqt" ];then
                 list_themes+=("Fusion" "Windows")
            else list_themes+=("fusion" "windows")
            fi
            #kvantum
            for _kvantum in ~/.local/share/Kvantum /usr/share/Kvantum; do
                if [ -d "$_kvantum" ]; then
                    for _dir in $(ls "$_kvantum" | sed "s/\n/ /g"); do
                        if [ -f "$_kvantum/$_dir/$_dir".kvconfig ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                fi
            done
            
            COMPREPLY=($(compgen -W "${list_themes[*]}" -- "${COMP_WORDS[4]}"))
        elif [ "${COMP_WORDS[3]}" == "wm-theme" ]; then
            local list_themes=()
            local valid_locations=()
            
            if [ "${COMP_WORDS[2]}" == "current" ];then
                return;
            
            elif [ "${COMP_WORDS[2]}" == "gnome" ];then
                #SAME CODE AS GNOME DESKTOP - has to have a gnome-shell folder in the theme.
                for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                    if [ -d "$_possible_locations" ]; then
                        valid_locations+=("$_possible_locations")
                    fi
                done
                
                for _valid in "${valid_locations[@]}"; do
                    for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                        if [ -d "$_valid/$_dir"/gnome-shell ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                done
                
            elif [ "${COMP_WORDS[2]}" == "plasma" ];then
                for _possible_locations in '/usr/share/aurorae/themes'; do
                    if [ -d "$_possible_locations" ]; then
                        valid_locations+=("$_possible_locations")
                    fi
                done
                
                for _valid in "${valid_locations[@]}"; do
                    for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                        if [ -f "$_valid"/"$_dir"/"$_dir"rc ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                done
                
            elif [ "${COMP_WORDS[2]}" == "xfce" ];then
                for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                    if [ -d "$_possible_locations" ]; then
                        valid_locations+=("$_possible_locations")
                    fi
                done
                
                for _valid in "${valid_locations[@]}"; do
                    for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                        if [ -f "$_valid/$_dir"/xfwm4/themerc ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                done
            #elif [ "${COMP_WORDS[2]}" == "cinnamon" ];then
            #elif [ "${COMP_WORDS[2]}" == "mate" ];then
            
            elif [ "${COMP_WORDS[2]}" == "lxqt" ];then
                for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                    if [ -d "$_possible_locations" ]; then
                        valid_locations+=("$_possible_locations")
                    fi
                done
                
                for _valid in "${valid_locations[@]}"; do
                    for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                        if [ -f "$_valid/$_dir"/openbox-3/themerc ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                done
            elif [ "${COMP_WORDS[2]}" == "budgie" ];then
                #same as gnome...
                for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                    if [ -d "$_possible_locations" ]; then
                        valid_locations+=("$_possible_locations")
                    fi
                done
                
                for _valid in "${valid_locations[@]}"; do
                    for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                        if [ -d "$_valid/$_dir"/gnome-shell ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                done
            fi
            COMPREPLY=($(compgen -W "${list_themes[*]}" -- "${COMP_WORDS[4]}"))
            
        elif [ "${COMP_WORDS[3]}" == "desktop-theme" ]; then
            local list_themes=()
            local valid_locations=()
            
            if [ "${COMP_WORDS[2]}" == "current" ];then
                return;
            
            elif [ "${COMP_WORDS[2]}" == "gnome" ];then
                #Has to have a gnome-shell folder in the theme.
                for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                    if [ -d "$_possible_locations" ]; then
                        valid_locations+=("$_possible_locations")
                    fi
                done
                
                for _valid in "${valid_locations[@]}"; do
                    for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                        if [ -d "$_valid/$_dir"/gnome-shell ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                done
                
            #elif [ "${COMP_WORDS[2]}" == "plasma" ];then
            #elif [ "${COMP_WORDS[2]}" == "xfce" ];then
            #elif [ "${COMP_WORDS[2]}" == "cinnamon" ];then
            #elif [ "${COMP_WORDS[2]}" == "mate" ];then
            #elif [ "${COMP_WORDS[2]}" == "lxqt" ];then
            
            elif [ "${COMP_WORDS[2]}" == "budgie" ];then
                #same as gnome...
                for _possible_locations in '/usr/share/themes' '~/.themes' '/usr/local/share/themes' '~/.local/share/themes'; do
                    if [ -d "$_possible_locations" ]; then
                        valid_locations+=("$_possible_locations")
                    fi
                done
                
                for _valid in "${valid_locations[@]}"; do
                    for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                        if [ -d "$_valid/$_dir"/gnome-shell ]; then
                            list_themes+=("$_dir")
                        fi
                    done
                done
            fi
        elif [ "${COMP_WORDS[3]}" == "wallpaper" ]; then
            local list_images=()
            for _file in $(ls); do
                # No directories. TODO
                case "$(file -b --mime-type $_file)" in
                    image*)
                        list_images+=("$_file")
                    ;;
                esac
            done
            COMPREPLY=($(compgen -W "${list_images[*]}" -- "${COMP_WORDS[4]}"))
            
        elif [ "${COMP_WORDS[3]}" == "icon-theme" ]; then
            local list_themes=()
            local valid_locations=()
                       
            for _possible_locations in '/usr/share/icons' '~/.icons' '/usr/local/share/icons' '~/.local/share/icons'; do
                if [ -d "$_possible_locations" ]; then
                    valid_locations+=("$_possible_locations")
                fi
            done

            for _valid in "${valid_locations[@]}"; do
                for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                    if [ -f "$_valid"/"$_dir"/icon-theme.cache ]; then
                        list_themes+=("$_dir")
                    elif ! [ -d "$_valid"/"$_dir"/cursors ]; then
                        if [ -f "$_valid"/"$_dir"/index.theme ]; then
                            list_themes+=("$_dir")
                        fi
                    fi
                done
            done

            COMPREPLY=($(compgen -W "${list_themes[*]}" -- "${COMP_WORDS[4]}"))
        
        elif [ "${COMP_WORDS[3]}" == "cursor-theme" ]; then
            local list_themes=()
            local valid_locations=()
                       
            for _possible_locations in '/usr/share/icons' '~/.icons' '/usr/local/share/icons' '~/.local/share/icons'; do
                if [ -d "$_possible_locations" ]; then
                    valid_locations+=("$_possible_locations")
                fi
            done

            for _valid in "${valid_locations[@]}"; do
                for _dir in $(ls "$_valid" | sed "s/\n/ /g"); do
                    if [ -d "$_valid"/"$_dir"/cursors ]; then
                        if [ -f "$_valid"/"$_dir"/index.theme ]; then
                            list_themes+=("$_dir")
                        fi
                    fi
                done
            done
            
            COMPREPLY=($(compgen -W "${list_themes[*]}" -- "${COMP_WORDS[4]}"))
        
#         elif [ "${COMP_WORDS[3]}" == "font" ]; then
        fi
    fi
}

complete -F _emilia_garden_completions emilia-garden
